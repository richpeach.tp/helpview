import UIKit

extension Screen {
    enum iPhone: String {
        case unknown
        case iPhone5
        case iPhone8
        case iPhonePlus
        case iPhoneXS
        case iPhoneXSMax
    }

    static var currentIPhone: iPhone {
        switch (width, height) {
            case (320, 568):
                return .iPhone5
            case (375, 667):
                return .iPhone8
            case (414, 736):
                return .iPhonePlus
            case (375, 812):
                return .iPhoneXS
            case (414, 896):
                return .iPhoneXSMax
            default:
                return .unknown
        }
    }

    static func isIn(iPhones: [iPhone]) -> Bool {
        let current = Screen.currentIPhone

        for iPhone in iPhones {
            if current.rawValue == iPhone.rawValue {
                return true
            }
        }

        return false
    }

    static var isIPhone5: Bool {
        return currentIPhone == .iPhone5
    }

    static var isBig: Bool {
        return Screen.isIn(iPhones: [.iPhonePlus, .iPhoneXS, .iPhoneXSMax])
    }

    static var isEdgeless: Bool {
        return Screen.isIn(iPhones: [.iPhoneXS, .iPhoneXSMax])
    }

}
