import UIKit

extension UIView {

    func setShadow(
        withColor color: UIColor = Palette.black,
        radius: CGFloat = 10,
        opacity: CGFloat = 1,
        offset: CGSize = .zero
    ) {
        layer.setShadow(withColor: color, radius: radius, opacity: opacity, offset: offset)
    }

    func setDefaultShadow() {
        setShadow(withColor: Palette.black, radius: 20, opacity: 0.56, offset: CGSize(width: 0, height: 10))
    }

}

extension CALayer {

    func setShadow(
        withColor color: UIColor = Palette.black,
        radius: CGFloat = 10,
        opacity: CGFloat = 1,
        offset: CGSize = .zero
    ) {
        shadowRadius = radius
        shadowOpacity = Float(opacity)
        shadowOffset = offset
        shadowColor = color.cgColor
    }

}
