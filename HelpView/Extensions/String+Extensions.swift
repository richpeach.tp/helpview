import UIKit

typealias Attributes = [NSAttributedString.Key: Any]

extension String {

    static func attributes(
        withFont font: UIFont? = nil,
        color: UIColor? = nil,
        lineSpacing: CGFloat? = nil,
        textAlignment: NSTextAlignment? = nil,
        kern: CGFloat? = nil
    ) -> [NSAttributedString.Key: Any] {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpacing ?? 0
        style.alignment = textAlignment ?? .natural

        var attributes: [NSAttributedString.Key: Any] = [:]

        if let font = font {
            attributes[NSAttributedString.Key.font] = font
        }

        attributes[NSAttributedString.Key.paragraphStyle] = style

        if let color = color {
            attributes[NSAttributedString.Key.foregroundColor] = color
        }

        if let kern = kern {
            attributes[NSAttributedString.Key.kern] = kern
        }

        return attributes
    }

    static func attributes(_ attributes: [NSAttributedString.Key: Any], font: UIFont? = nil, color: UIColor? = nil) -> [NSAttributedString.Key: Any] {
        var copy = attributes

        if let font = font {
            copy[NSAttributedString.Key.font] = font
        }

        if let color = color {
            copy[NSAttributedString.Key.foregroundColor] = color
        }

        return copy
    }

}

extension String {

    func withAttributes(_ attributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: attributes)
    }

    func withAttributes(
        font: UIFont? = nil,
        color: UIColor? = nil,
        lineSpacing: CGFloat? = nil,
        textAlignment: NSTextAlignment? = nil,
        kern: CGFloat? = nil
    ) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: String.attributes(withFont: font, color: color, lineSpacing: lineSpacing, textAlignment: textAlignment, kern: kern))
    }

    func size(
        for width: CGFloat,
        font: UIFont? = nil,
        lineSpacing: CGFloat? = nil,
        textAlignment: NSTextAlignment? = nil,
        kern: CGFloat? = nil
    ) -> CGSize {
        return NSAttributedString(string: self, attributes: String.attributes(withFont: font, lineSpacing: lineSpacing, textAlignment: textAlignment, kern: kern)).boundingSize(for: width)
    }

}

import UIKit

extension String {

    func boundingSize(with size: CGSize, attributes: [NSAttributedString.Key: Any]?) -> CGSize {
        let string = self as NSString

        let options: NSStringDrawingOptions = [.usesLineFragmentOrigin, .usesFontLeading]
        var rect = string.boundingRect(with: size, options: options, attributes: attributes, context: nil)
        rect = rect.integral

        return rect.size
    }

    func boundingSize(forWidth width: CGFloat, attributes: [NSAttributedString.Key: Any]?) -> CGSize {
        return boundingSize(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude),
                            attributes: attributes)
    }

}

extension NSAttributedString {

    func boundingSize(with size: CGSize) -> CGSize {

        let options: NSStringDrawingOptions = [.usesLineFragmentOrigin, .usesFontLeading]
        var rect = boundingRect(with: size, options: options, context: nil)
        rect = rect.integral

        return rect.size
    }

    func boundingSize(for width: CGFloat) -> CGSize {
        return boundingSize(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude))
    }

}

extension UILabel {

    func boundingSize(for width: CGFloat) -> CGSize {
        text?.withAttributes(font: font, color: textColor).boundingSize(for: width) ?? .zero
    }

}

extension String {

    func underlinedText(withColor color: UIColor, font: UIFont) -> NSAttributedString {
        withAttributes(
            [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .font: font,
                .foregroundColor: color
            ]
        )
    }

}

extension String {

    static func attributedTitle(_ title: String, isSelected: Bool, isCaps: Bool) -> NSAttributedString {
        let strokeTextAttributes = [
            NSAttributedString.Key.strokeColor : Palette.black,
            NSAttributedString.Key.foregroundColor : isSelected ? Palette.black : .clear,
            NSAttributedString.Key.strokeWidth : isSelected ? 0 : -4.0,
            NSAttributedString.Key.font : isCaps ? Font.caps : Font.intro,
            NSAttributedString.Key.kern: 5
        ] as [NSAttributedString.Key : Any]

        return NSAttributedString(string: title, attributes: strokeTextAttributes)
    }

}
