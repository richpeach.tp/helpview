import UIKit

extension UINavigationBar {

    func makeTransparent() {
        setBackgroundImage(UIImage(), for: .default)
        barTintColor = nil
        shadowImage = UIImage()
        isTranslucent = true
    }

    func fill(
        backgroundColor: UIColor = Palette.primary,
        tintColor: UIColor = Palette.white
    ) {
        isTranslucent = false
        barTintColor = backgroundColor
        self.tintColor = tintColor
    }

}

extension UIBarButtonItem {

    static func backItem(target: Any?, action: Selector?) -> UIBarButtonItem {
        UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: target, action: action)
    }

    static func saveItem(target: Any?, action: Selector?) -> UIBarButtonItem {
        UIBarButtonItem(image: UIImage(named: "tick"), style: .done, target: target, action: action)
    }

}

extension UIViewController {

    func setupNavigationBar(backButtonAction: Selector? = nil) {
        navigationItem.leftBarButtonItem = UIBarButtonItem.backItem(target: self, action: backButtonAction)
        navigationItem.backBarButtonItem = UIBarButtonItem.backItem(target: self, action: backButtonAction)
        navigationController?.interactivePopGestureRecognizer?.delegate = nil

        navigationController?.navigationBar.titleTextAttributes = [
            .font: Font.regular.withSize(18),
            .foregroundColor: Palette.white
        ]

        navigationController?.navigationBar.backIndicatorImage = UIImage()
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
    }

}
