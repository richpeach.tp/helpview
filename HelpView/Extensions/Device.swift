import UIKit

struct Device {
    static let id = UIDevice.current.identifierForVendor?.uuidString
    static let model = UIDevice.current.localizedModel
    static let system = UIDevice.current.systemVersion
}

struct App {
    static let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    static let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
}
