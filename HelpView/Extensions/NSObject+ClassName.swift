import Foundation

extension NSObject {

    class var className: String {
        guard let result = String(describing: self).components(separatedBy: ".").last else {
            return String(describing: self)
        }

        return result
    }

    var className: String {
        return type(of: self).className
    }

}
