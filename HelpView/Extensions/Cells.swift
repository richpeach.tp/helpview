import UIKit

//extension UICollectionViewCell {
//
//    class var reuseIdentifier: String {
//        return className
//    }
//
//}

extension UITableViewCell {

    class var reuseIdentifier: String {
        return className
    }

}

extension UITableViewHeaderFooterView {

    class var reuseIdentifier: String {
        return className
    }

}

extension UICollectionReusableView {

    class var reuseIdentifier: String {
        return className
    }

}


