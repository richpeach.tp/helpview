import UIKit

extension InputCell {

    struct ViewModel {
        let title: String?
        let text: String?
        let placeholder: String?
        let isMultiline: Bool

        var reduce: ((_ text: String, _ range: NSRange, _ replacementString: String) -> String)? = nil

        var isEmpty: Bool {
            text == nil || text?.isEmpty == true
        }
    }

}

class InputCell: UICollectionViewCell {

    var onInputChange: ((InputCell) -> Void)?

    var inputText: String {
        textView.text ?? ""
    }

    // MARK: - Views

    lazy var titleLabel = UILabel(text: nil, textColor: Constants.titleColor, font: Constants.titleFont)

    lazy var textView: KMPlaceholderTextView = {
        let textView = KMPlaceholderTextView()
        textView.font = Constants.textFont
        textView.textColor = Constants.textColor
        textView.textContainer.lineFragmentPadding = 0
        textView.textContainerInset = .zero
        textView.backgroundColor = .clear
        textView.delegate = self
        textView.isScrollEnabled = false
        textView.placeholderFont = Constants.placeholderFont
        textView.placeholderColor = Constants.placeholderColor
        return textView
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        backgroundColor = Palette.white

        addSubviews([titleLabel, textView])

        roundize(cornerRadius: 15, clips: true)
    }

    // MARK: - Layout

    static func height(for model: ViewModel, maximumWidth: CGFloat) -> CGFloat {
        let insets = Constants.insets

        let width = maximumWidth - Constants.insets.horizontal
        let titleHeight = model.title?.withAttributes(font: Constants.titleFont).boundingSize(for: width).height ?? 0
        let text = !model.isEmpty ? model.text! : " "
        let textHeight = text
            .withAttributes(font: Constants.textFont)
            .boundingSize(for: width).height

        return insets.vertical
            + (titleHeight > 0 ? titleHeight + Constants.titleToText : 0)
            + textHeight
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let insets = Constants.insets

        if viewModel?.title != nil {
            titleLabel.pin
                .top(insets.top)
                .left(insets.left)
                .right(insets.right)
                .sizeToFit()

            textView.pin
                .below(of: titleLabel)
                .marginTop(Constants.titleToText)
                .bottom(insets.bottom)
                .left(insets.left)
                .right(insets.right)
        } else {
            textView.pin.all(insets)
        }
    }

    // MARK: - Actions

    // MARK: - View Model

    var viewModel: ViewModel? {
        didSet {
            titleLabel.text = viewModel?.title
            textView.text = viewModel?.text
            textView.placeholder = viewModel?.placeholder ?? ""
            setNeedsLayout()
        }
    }

}

extension InputCell: UITextViewDelegate {

    func textViewDidChange(_ textView: UITextView) {
        handleTextChange()
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" && viewModel?.isMultiline == false {
            textView.resignFirstResponder()
            return false
        }

        let changedText: String
        let selectedRange = textView.position(from: textView.beginningOfDocument, offset: range.location + text.count)

        if let reduce = viewModel?.reduce {
            changedText = reduce(textView.text, range, text)
        } else {
            changedText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        }

        textView.text = changedText
        handleTextChange()

        if let selectedRange = selectedRange {
            textView.selectedTextRange = textView.textRange(from: selectedRange, to: selectedRange)
        }

        return false
    }

    // MARK: - Layout

    private func handleTextChange() {
        onInputChange?(self)
    }

}


private enum Constants {

    static let insets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)

    static let titleFont = Font.bold.withSize(14)
    static let titleColor = Palette.black

    static let textFont = Font.regular.withSize(14)
    static let textColor = Palette.black

    static let placeholderFont = Font.regular.withSize(14)
    static let placeholderColor = Palette.gray

    static let titleToText: CGFloat = 8

}
