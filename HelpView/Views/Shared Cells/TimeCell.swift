import UIKit

extension TimeCell {

    struct ViewModel {
        let title: String
        let text: String?

        var isEmpty: Bool {
            text == nil || text?.isEmpty == true
        }
    }

}

class TimeCell: UICollectionViewCell {

    // MARK: - Views

    lazy var titleLabel = UILabel(text: nil, textColor: Constants.titleColor, font: Constants.titleFont)

    lazy var textLabel = UILabel(text: nil, textColor: Constants.textColor, font: Constants.textFont)

    lazy var placeholderLabel = UILabel(text: "Tap to change", textColor: Constants.placeholderColor, font: Constants.placeholderFont)

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        backgroundColor = Palette.white

        addSubviews([titleLabel, textLabel, placeholderLabel])

        roundize(cornerRadius: 15, clips: true)
    }

    // MARK: - Layout

    static func height(for model: ViewModel, maximumWidth: CGFloat) -> CGFloat {
        let insets = Constants.insets

        let width = maximumWidth - Constants.insets.horizontal
        let titleHeight = model.title.withAttributes(font: Constants.titleFont).boundingSize(for: width).height
        let text = !model.isEmpty ? model.text! : " "
        let textHeight = text
            .withAttributes(font: Constants.textFont)
            .boundingSize(for: width).height

        return insets.vertical + titleHeight + Constants.titleToText + textHeight
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let insets = Constants.insets

        titleLabel.pin
            .top(insets.top)
            .left(insets.left)
            .right(insets.right)
            .sizeToFit()

        textLabel.pin
            .below(of: titleLabel)
            .marginTop(Constants.titleToText)
            .bottom(insets.bottom)
            .left(insets.left)
            .sizeToFit()

        if textLabel.isHidden {
            placeholderLabel.pin
                .left(insets.left)
                .bottom(insets.bottom)
                .sizeToFit()
        } else {
            placeholderLabel.pin
                .after(of: textLabel)
                .sizeToFit()
                .bottom(insets.bottom + 1)
                .marginLeft(Constants.titleToText)
        }

    }

    // MARK: - Actions

    // MARK: - View Model

    var viewModel: ViewModel? {
        didSet {
            titleLabel.text = viewModel?.title
            textLabel.text = viewModel?.text

            if viewModel?.isEmpty == true {
                textLabel.isHidden = true
            } else {
                textLabel.isHidden = false
            }

            setNeedsLayout()
        }
    }

}

private enum Constants {

    static let insets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)

    static let titleFont = Font.bold.withSize(14)
    static let titleColor = Palette.black

    static let textFont = Font.bold.withSize(14)
    static let textColor = Palette.primary

    static let placeholderFont = Font.regular.withSize(14)
    static let placeholderColor = Palette.gray

    static let titleToText: CGFloat = 8

}
