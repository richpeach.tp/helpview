import UIKit

class DotsView: UIView {

    var count: Int = 0 {
        didSet {
            render()
        }
    }

    var selectedIndex: Int = 0 {
        didSet {
            render()
        }
    }

    private var dots: [DotView] = []

    private let containerView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(containerView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Render

    private func render() {
        dots.forEach { $0.removeFromSuperview() }

        dots = generateDots()
        dots.forEach(containerView.addSubview)

        setNeedsLayout()
    }

    private func generateDots() -> [DotView] {
        var dots: [DotView] = []

        for i in 0..<count {
            let dot = DotView()
            dot.isSelected = i == selectedIndex
            dots.append(dot)
        }

        return dots
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        for (i, dot) in dots.enumerated() {
            dot.pin
                .size(Constants.dotSize)
                .vCenter()
                .left((Constants.dotSize.width + Constants.dotSpacing) * CGFloat(i))

            dot.roundize()
        }

        _ = dots.last.map {
            containerView.pin
                .width($0.frame.maxX)
                .height($0.frame.height)
                .center()
        }
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let dotSize = Constants.dotSize
        let fCount = CGFloat(count)
        let maxX = fCount * dotSize.width + (fCount - 1) * Constants.dotSpacing
        return CGSize(width: maxX, height: dotSize.height)
    }

}

private enum Constants {

    static let dotSize = CGSize(side: 10)
    static let dotSpacing: CGFloat = 6
    static let dotColor = Palette.white

}
