import UIKit

class DotView: UIView {

    var isSelected: Bool = false {
        didSet {
            centerDotView.isHidden = isSelected
        }
    }

    private lazy var centerDotView = UIView(backgroundColor: Palette.primary)

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(centerDotView)
        backgroundColor = Palette.white
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        centerDotView.pin
            .size(Constants.centerDotSize)
            .center()

        centerDotView.roundize()
        roundize()
    }

}

private enum Constants {
    static let centerDotSize = CGSize(side: 3)
}
