import UIKit
import UserNotifications

protocol NotificationServiceProtocol {

    var isEnabled: Bool { get }

    func setEnabled(_ enabled: Bool)

    func schedulePost(withId id: String, at date: Date, onSuccess: VoidHandler?)

    func cancelNotification(withId id: String)

}

class NotificationService: NotificationServiceProtocol {

    var isEnabled: Bool {
        get {
            userSettings.isNotificationEnabled
        }
        set {
            userSettings.isNotificationEnabled = newValue
        }
    }

    private let notificationCenter = UNUserNotificationCenter.current()

    private var userSettings: UserSettingsProtocol

    private let spaceService: SpaceServiceProtocol

    unowned var feedService: FeedServiceProtocol!

    init(userSettings: UserSettingsProtocol, spaceService: SpaceServiceProtocol) {
        self.userSettings = userSettings
        self.spaceService = spaceService
    }

    // MARK: - Authorization

    func requestAuthorization(onGrant: @escaping VoidHandler, onDeny: @escaping VoidHandler) {
        notificationCenter.getNotificationSettings { [weak self] settings in
            let firstTime = settings.authorizationStatus == .notDetermined

            self?.notificationCenter.requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
                DispatchQueue.main.async {
                    if granted {
                        print("All set!")
                        if firstTime {
                            self?.isEnabled = true
                        }
                        onGrant()
                    } else if !granted {
                        onDeny()
                    } else if let error = error {
                        print(error.localizedDescription)
                        onDeny()
                    }
                }
            }
        }

    }

    func setEnabled(_ enabled: Bool) {
        isEnabled = enabled

        if !isEnabled {
            cancelAllPendingNotifications()
        } else {
            let posts = feedService.allPostsWithPublishDate(from: spaceService.spaces)

            posts.forEach { post in
                if let publishDate = post.publishDate {
                    schedulePost(withId: post.id, at: publishDate, onSuccess: nil)
                }
            }
        }
    }

    // MARK: - Schedule

    func schedulePost(withId id: String, at date: Date, onSuccess: VoidHandler?) {
        requestAuthorization(
            onGrant: {
                self.cancelNotification(withId: id)

                if self.isEnabled {
                    self.addNotificationRequest(withId: id, at: date)
                }

                onSuccess?()
            },
            onDeny: {
                AlertActionInteraction(
                    title: "Sorry",
                    message: "We do not have permission to send you notifications. These can be configured in Settings.",
                    action: UIAlertAction(title: "Settings", style: .default, handler: { _ in
                        SettingsInteraction().follow()
                    })
                ).follow()
            }
        )
    }

    private func addNotificationRequest(withId id: String, at date: Date) {
        let content = UNMutableNotificationContent()
        content.subtitle = "It's time to post!"
        content.sound = UNNotificationSound.default

        let trigger = UNCalendarNotificationTrigger(
            dateMatching: Calendar.current.dateComponents([.day, .hour, .minute], from: date),
            repeats: false
        )

        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)

        notificationCenter.add(request)
    }

    func cancelNotification(withId id: String) {
        notificationCenter.removePendingNotificationRequests(withIdentifiers: [id])
    }

    func cancelAllPendingNotifications() {
        notificationCenter.removeAllPendingNotificationRequests()
    }

}

