import Foundation

class Post: Codable, Equatable {

    var id: String {
        date.timeIntervalSince1970.description
    }

    let image: Image
    let date: Date

    var publishDate: Date?
    
    var caption: String?

    var hashtags: [String] {
        let caption = caption ?? ""
        let components = caption.components(separatedBy: .whitespacesAndNewlines)
        return components.filter { $0.hasPrefix("#") }
    }

    init(image: Image, date: Date = Date()) {
        self.image = image
        self.date = date
        self.publishDate = nil
    }

    static func == (lhs: Post, rhs: Post) -> Bool {
        lhs.image == rhs.image && lhs.date == rhs.date
    }

}

