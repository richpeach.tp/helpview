import Foundation

protocol FeedProtocol {

    var kind: Feed.Kind { get }

    var posts: [Post] { get }

    var spaceId: String { get }

    func insert(_ posts: [Post], at index: Int)

    func insert(_ post: Post, at index: Int)

    func insert(_ post: Post)

    func remove(_ post: Post)

    func movePost(from index: Int, to destination: Int)

    func saveToDisk()

}

class Feed: FeedProtocol {

    enum Kind: String {
        case posts
        case stories
    }

    let spaceId: String

    let kind: Kind

    let notificationService: NotificationServiceProtocol

    private(set) var posts: [Post] {
        didSet {
            saveToDisk()
        }
    }

    init(spaceId: String, kind: Kind, notificationService: NotificationServiceProtocol, posts: [Post]? = nil) {
        self.kind = kind
        self.notificationService = notificationService
        self.spaceId = spaceId

        if let posts = posts {
            self.posts = posts
        } else {
            self.posts = Storage.retrieve(Self.diskKey(spaceId: spaceId, kind: kind), from: .documents, as: [Post].self) ?? []
        }
    }

    func insert(_ post: Post) {
        self.insert(post, at: 0)
    }

    func insert(_ post: Post, at index: Int) {
        posts.insert(post, at: index)
    }

    func insert(_ posts: [Post], at index: Int) {
        posts.forEach { post in
            if let publishDate = post.publishDate {
                notificationService.schedulePost(withId: post.id, at: publishDate, onSuccess: nil)
            }
        }

        self.posts.insert(contentsOf: posts, at: index)
    }

    func remove(_ post: Post) {
        notificationService.cancelNotification(withId: post.id)
        posts.removeAll { $0 == post }
    }

    func movePost(from index: Int, to destination: Int) {
        let post = posts.remove(at: index)
        posts.insert(post, at: destination)
    }

}

extension Feed {

    // MARK: - Storage

    static func diskKey(spaceId: String, kind: Kind) -> String {
        spaceId + "_" + kind.rawValue + ".json"
    }

    private var diskKey: String {
        Self.diskKey(spaceId: spaceId, kind: kind)
    }

    func saveToDisk() {
        Storage.queue.async { [weak self] in
            guard let self = self else { return }
            Storage.store(self.posts, to: .documents, as: self.diskKey)
        }
    }

}
