import UIKit

extension Post {

    struct Image: Codable, Equatable {
        let key: FileKey
        let size: CGSize
    }

}
