import UIKit

protocol FeedServiceProtocol: AnyObject {

    func feed(with spaceId: Space.ID, andKind kind: Feed.Kind) -> FeedProtocol

    func allPostsWithPublishDate(from spaces: [Space]) -> [Post]

    var feeds: [FeedProtocol] { get }

}

class FeedService: FeedServiceProtocol {

    let notificationService: NotificationServiceProtocol

    init(notificationService: NotificationServiceProtocol) {
        self.notificationService = notificationService
    }

    var feeds: [FeedProtocol] = []

    func feed(with spaceId: Space.ID, andKind kind: Feed.Kind) -> FeedProtocol {
        if let foundFeed = feeds.first(where: { $0.kind == kind && $0.spaceId == spaceId }) {
            return foundFeed
        }

        let newFeed = Feed(spaceId: spaceId, kind: kind, notificationService: notificationService)
        feeds.append(newFeed)

        return newFeed
    }

    //

    func allPostsWithPublishDate(from spaces: [Space]) -> [Post] {
        let feeds = spaces.map { feed(with: $0.id, andKind: .posts) }
        return feeds.map { $0.posts }.reduce([], +).filter { $0.publishDate != nil }
    }

}
