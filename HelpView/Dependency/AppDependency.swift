struct AppDependency {
    var spaceService: SpaceServiceProtocol
    let feedService: FeedServiceProtocol
    let imageStorage: ImageStorageProtocol
    var userSettings: UserSettingsProtocol
    let notificationService: NotificationServiceProtocol
}
