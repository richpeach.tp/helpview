import Foundation

protocol UserSettingsProtocol {

    var isPremium: Bool { get set }

    var isFirstLaunch: Bool { get set }

    var isNotificationEnabled: Bool { get set }

    // Data

    func set(data: Data, forKey key: String)
    func data(forKey key: String) -> Data?

    // String

    func set(strings: [String], forKey key: String)
    func strings(forKey key: String) -> [String]

    // Value

    func set(value: Any?, forKey key: String)
    func value(forKey key: String) -> Any?
}
