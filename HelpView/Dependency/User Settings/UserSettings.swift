import Foundation

class UserSettings: UserSettingsProtocol {

    let userDefaults: UserDefaults

    init(userDefaults: UserDefaults = .standard) {
        self.userDefaults = userDefaults
    }

    // MARK: - String

    func set(strings: [String], forKey key: String) {
        userDefaults.setValue(strings, forKey: key)
    }

    func strings(forKey key: String) -> [String] {
        userDefaults.stringArray(forKey: key) ?? []
    }

    // MARK: - Data

    func set(data: Data, forKey key: String) {
        userDefaults.set(data, forKey: key)
    }

    func data(forKey key: String) -> Data? {
        return userDefaults.data(forKey: key)
    }

    //

    func set(value: Any?, forKey key: String) {
        userDefaults.setValue(value, forKey: key)
    }

    func value(forKey key: String) -> Any? {
        userDefaults.value(forKey: key)
    }

    //

    var isPremium: Bool {
        get {
            userDefaults.bool(forKey: "UserSettings.isPremium")
        }
        set {
            userDefaults.setValue(newValue, forKey: "UserSettings.isPremium")
        }
    }

    var isNotificationEnabled: Bool {
        get {
            userDefaults.bool(forKey: "UserSettings.isNotificationEnabled")
        }
        set {
            userDefaults.setValue(newValue, forKey: "UserSettings.isNotificationEnabled")
        }
    }

    var isFirstLaunch: Bool {
        get {
            let value = userDefaults.value(forKey: "UserSettings.isFirstLaunch") as? Bool
            return value == nil || value == true
        }
        set {
            userDefaults.setValue(newValue, forKey: "UserSettings.isFirstLaunch")
        }
    }


}
