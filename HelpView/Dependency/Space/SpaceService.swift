class SpaceService: SpaceServiceProtocol {

    init() {
        if let spaces = Storage.retrieve(Constants.diskKey, from: .documents, as: [Space].self), !spaces.isEmpty {
            self.spaces = spaces
        } else {
            self.spaces = [.default]
        }

        self.current = self.spaces.first!
    }

    var current: Space {
        didSet {
            notifications.forEach { $0() }
        }
    }

    var spaces: [Space] {
        didSet {
            saveToDisk()
        }
    }

    var notifications: [VoidHandler] = []

    func addSpace(name: String) -> Space {
        let space = Space(title: name)
        spaces.append(space)
        return space
    }

    func notifyOnCurrentSpaceChange(handler: @escaping VoidHandler) {
        notifications.append(handler)
    }

}

extension SpaceService {

    // MARK: - Storage

    private func saveToDisk() {
        Storage.store(spaces, to: .documents, as: Constants.diskKey)
    }

}

private enum Constants {
    static let diskKey = "spaces.json"
}
