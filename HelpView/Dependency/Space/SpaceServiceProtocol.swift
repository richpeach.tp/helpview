protocol SpaceServiceProtocol {

    var current: Space { get set }

    var spaces: [Space] { get }

    func addSpace(name: String) -> Space

    func notifyOnCurrentSpaceChange(handler: @escaping VoidHandler)

}
