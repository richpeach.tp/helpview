struct Space: Codable, Equatable {

    typealias ID = String
    
    let title: String

    var id: String {
        title
    }

}

extension Space {

    static let `default` = Space(title: "Default Space")

}
