import UIKit

protocol ImageStorageProtocol {

    func store(_ image: UIImage, forKey key: FileKey)

    func retrieveImage(forKey key: FileKey) -> UIImage?

    func retrieveImage(forKey key: FileKey, completion: @escaping ((UIImage?) -> Void))

}

class ImageStorage {

    let fileStorage = FileStorage()

    private var cache: [String: UIImage] = [:]

}

extension ImageStorage: ImageStorageProtocol {

    func store(_ image: UIImage, forKey key: FileKey) {
        if let pngRepresentation = image.pngData(),
           let filePath = fileStorage.pngFilePath(forKey: key) {
            do {
                try pngRepresentation.write(
                    to: filePath,
                    options: .atomic
                )
            } catch let err {
                print("Saving file resulted in error: ", err)
            }
        }
    }

    func retrieveImage(forKey key: FileKey) -> UIImage? {
        if let cachedImage = cache[key] {
            return cachedImage
        }

        guard let pngData = fileStorage.pngFileData(forKey: key) else { return nil }

        let image = UIImage(data: pngData)

        DispatchQueue.main.async {
            self.cache[key] = image
        }

        return image
    }

    func retrieveImage(forKey key: FileKey, completion: @escaping ((UIImage?) -> Void)) {
        DispatchQueue.global(qos: .userInteractive).async {
            let image = self.retrieveImage(forKey: key)

            DispatchQueue.main.async {
                completion(image)
            }
        }
    }

}
