import Foundation

typealias FileKey = String
class FileStorage {

    // MARK: - Data

    func fileData(atPath path: String) -> Data? {
        FileManager.default.contents(atPath: path)
    }

    // MARK: - Path

    func filePath(forKey key: FileKey) -> URL? {
        let fileManager = FileManager.default

        guard let documentURL = fileManager.urls(
            for: .documentDirectory,
            in: FileManager.SearchPathDomainMask.userDomainMask
        ).first else { return nil }

        return documentURL.appendingPathComponent(key)
    }

}

extension FileStorage {

    // MARK: - PNG

    func pngFileData(forKey key: FileKey) -> Data? {
        guard let filePath = pngFilePath(forKey: key) else { return nil }
        return fileData(atPath: filePath.path)
    }

    func pngFilePath(forKey key: FileKey) -> URL? {
        filePath(forKey: key + ".png")
    }

}
