import Photos

final class PhotoLibraryPermissionsService: NSObject {

    // MARK: - Status

    typealias AuthorizationStatus = PHAuthorizationStatus

    var readWriteAuthorizationStatus: AuthorizationStatus {
        PHPhotoLibrary.readWriteAuthorizationStatus()
    }

}

// MARK: - Access

extension PhotoLibraryPermissionsService {

    func requestReadWriteAccess(onGrant: @escaping VoidHandler, onDeny: @escaping VoidHandler) {
        switch readWriteAuthorizationStatus {
        case .authorized, .limited:
            onGrant()

        case .restricted, .denied:
            onDeny()

        case .notDetermined:
            requestReadWriteAuthorization { status in
                switch status {
                case .authorized, .limited:
                    onGrant()

                case .denied, .restricted:
                    onDeny()

                case .notDetermined:
                    fatalError("impossible case")

                @unknown default:
                    onDeny()
                }
            }
        @unknown default:
            onDeny()
        }
    }

    private func requestReadWriteAuthorization(completion: @escaping ((AuthorizationStatus) -> Void)) {
        PHPhotoLibrary.requestReadWriteAuthorization { status in
            DispatchQueue.main.async {
                completion(status)
            }
        }
    }

}

extension PHPhotoLibrary {

    class func requestReadWriteAuthorization(handler: @escaping (PHAuthorizationStatus) -> Void) {
        if #available(iOS 14.0, *) {
            requestAuthorization(for: .readWrite, handler: handler)
        } else {
            requestAuthorization(handler)
        }
    }

    class func readWriteAuthorizationStatus() -> PHAuthorizationStatus {
        if #available(iOS 14.0, *) {
            return authorizationStatus(for: .readWrite)
        } else {
            return authorizationStatus()
        }
    }

}
