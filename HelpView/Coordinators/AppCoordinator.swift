import UIKit

class AppCoordinator: Coordinator {

    let window: UIWindow

    var appDependency: AppDependency

    // MARK: - Init

    init(window: UIWindow) {
        self.window = window

        let spaceService = SpaceService()
        let userSettings = UserSettings()
        let notificationService = NotificationService(userSettings: userSettings, spaceService: spaceService)
        let feedService = FeedService(notificationService: notificationService)
        notificationService.feedService = feedService
        
        self.appDependency = AppDependency(
            spaceService: spaceService,
            feedService: feedService,
            imageStorage: ImageStorage(),
            userSettings: userSettings,
            notificationService: notificationService
        )

        super.init()
    }

    // MARK: - Coordinator

    override func start() {
        if appDependency.userSettings.isFirstLaunch {
            transition(to: .onboarding)
        } else {
            transition(to: .feed)
        }
    }

    // MARK: - Navigation

    enum Page {
        case onboarding
        case feed
    }

    var page: Page?

    private func transition(to page: Page) {
        switch page {
        case .onboarding:
            openOnboarding()

        case .feed:
            if self.page == .onboarding {
                animateWindowSwitching()
            }
            openFeed()
        }

        window.makeKeyAndVisible()

        self.page = page
    }

    private func openOnboarding() {
        let onboardingViewController = OnboardingViewController()
        onboardingViewController.onFinish = { [unowned self] in
            appDependency.userSettings.isFirstLaunch = false
            transition(to: .feed)
        }
        window.rootViewController = onboardingViewController
    }

    private func openFeed() {
        let feedCoordinator = FeedCoordinator(window: window, appDependency: appDependency)
        add(feedCoordinator)

        feedCoordinator.start()
    }

    // MARK: - Helpers

    private func animateWindowSwitching() {
        UIView.transition(
            with: window,
            duration: 0.2,
            options: .transitionCrossDissolve,
            animations: nil,
            completion: nil
        )
    }

}
