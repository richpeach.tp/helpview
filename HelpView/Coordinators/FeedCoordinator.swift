import UIKit

class FeedCoordinator: Coordinator {

    let window: UIWindow

    var appDependency: AppDependency

    var navigationController: UINavigationController?

    // MARK: - Init

    init(window: UIWindow, appDependency: AppDependency) {
        self.window = window
        self.appDependency = appDependency
        super.init()
    }

    // MARK: - Coordinator

    override func start() {
        super.start()

        transition(to: .feed)
    }

    // MARK: - Navigation

    enum Page {
        case feed
        case calendar
        case post(FeedProtocol, Post)
        case addSpace
        case split(FeedProtocol, Post)
        case hashtags(FeedProtocol, Post)
        case settings(onlyPremium: Bool)
        case postList(Date, [Post])
    }

    private func transition(to page: Page) {
        switch page {
        case .feed:
            openFeed()

        case .calendar:
            let calendarViewController = CalendarViewController(currentSpace: appDependency.spaceService.current, feedService: appDependency.feedService)

            calendarViewController.onPostsTap = { [unowned self] date, posts in
                if posts.count > 1 {
                    transition(to: .postList(date, posts))
                } else {
                    let post = posts.first!

                    appDependency.feedService.feeds.first { $0.posts.contains(post) }.map {
                        transition(to: .post($0, post))
                    }
                }
            }

            calendarViewController.onBackTap = { [unowned self] in
                navigationController?.popViewController(animated: true)
            }

            navigationController?.pushViewController(calendarViewController, animated: true)

        case .postList(let date, let posts):
            let postListViewController = PostListViewController(date: date, posts: posts, imageStorage: appDependency.imageStorage)

            postListViewController.onTap = { [unowned self] post in
                appDependency.feedService.feeds.first { $0.posts.contains(post) }.map {
                    transition(to: .post($0, post))
                }
            }

            postListViewController.onBackTap = { [unowned self] in
                navigationController?.popViewController(animated: true)
            }

            navigationController?.pushViewController(postListViewController, animated: true)

        case .post(let feed, let post):
            let postViewController = PostViewController(post: post, imageStorage: appDependency.imageStorage, notificationService: appDependency.notificationService)

            postViewController.onSave = { [unowned self] in
                feed.saveToDisk()
                navigationController?.popToRootViewController(animated: true)
            }

            postViewController.onDeleteTap = { [unowned self] in
                feed.remove(post)
                navigationController?.popViewController(animated: true)
            }

            navigationController?.pushViewController(postViewController, animated: true)

        case .addSpace:
            let newSpaceViewController = NewSpaceViewController(spaceService: appDependency.spaceService)

            newSpaceViewController.onAddSpace = { [unowned self] name in
                let newSpace = appDependency.spaceService.addSpace(name: name)
                appDependency.spaceService.current = newSpace
                navigationController?.popToRootViewController(animated: true)
            }

            navigationController?.pushViewController(newSpaceViewController, animated: true)

        case .split(let feed, let post):
            guard let image = appDependency.imageStorage.retrieveImage(forKey: post.image.key) else { return }

            let splitViewController = SplitViewController(feed: feed, post: post, image: image, imageStorage: appDependency.imageStorage)

            splitViewController.onSave = { [unowned self] in
                navigationController?.popToRootViewController(animated: true)
            }

            navigationController?.pushViewController(splitViewController, animated: true)

        case .hashtags(let feed, let post):
            let hashtagsViewController = HashtagsViewController(post: post)

            hashtagsViewController.onSave = { [unowned self] in
                feed.saveToDisk()
                navigationController?.popToRootViewController(animated: true)
            }

            navigationController?.pushViewController(hashtagsViewController, animated: true)

        case .settings(let onlyPremium):
            guard let navigationController = navigationController else { return }

            let coordinator = SettingsCoordinator(appDependency: appDependency, onlyPremium: onlyPremium, navigationController: navigationController)
            add(coordinator)
            coordinator.start()

        }
    }

    private func openFeed() {
        let feedViewController = FeedViewController(
            spaceService: appDependency.spaceService,
            feedService: appDependency.feedService,
            imageStorage: appDependency.imageStorage,
            userSettings: appDependency.userSettings
        )

        feedViewController.onOpenPostTap = { [unowned self] feed, post in
            transition(to: .post(feed, post))
        }

        feedViewController.onAddSpaceTap = { [unowned self] in
            if appDependency.userSettings.isPremium {
                transition(to: .addSpace)
            } else {
                transition(to: .settings(onlyPremium: true))
            }
        }

        feedViewController.onCalendarTap = { [unowned self] in
            transition(to: .calendar)
        }

        feedViewController.onPremium = { [unowned self] in
            transition(to: .settings(onlyPremium: true))
        }

        feedViewController.onSplitTap = { [unowned self] feed, post in
            if appDependency.userSettings.isPremium {
                transition(to: .split(feed, post))
            } else {
                transition(to: .settings(onlyPremium: true))
            }
        }

        feedViewController.onHashtagTap = { [unowned self] feed, post in
            transition(to: .hashtags(feed, post))
        }

        feedViewController.onSettingsTap = { [unowned self] in
            transition(to: .settings(onlyPremium: false))
        }

        navigationController = UINavigationController(rootViewController: feedViewController)
        window.rootViewController = navigationController!
    }

}

