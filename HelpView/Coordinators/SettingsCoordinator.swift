import UIKit
import MessageUI

class SettingsCoordinator: Coordinator {

    enum Page {
        case settings
        case premium
        case terms
        case privacy
        case support
        case notification
    }

    let appDependency: AppDependency
    let onlyPremium: Bool

    init(appDependency: AppDependency, onlyPremium: Bool = false, navigationController: UINavigationController) {
        self.appDependency = appDependency
        self.onlyPremium = onlyPremium
        self.navigationController = navigationController
        super.init()
    }

    // MARK: - Coordinator

    override func start() {
        super.start()

        if onlyPremium {
            transition(to: .premium)
        } else {
            transition(to: .settings)
        }
    }

    // MARK: - Navigation

    private var navigationController: UINavigationController

    private var page: Page?

    private func transition(to page: Page) {
        switch page {
        case .settings:
            let settingsViewController = SettingsViewController(userSettings: appDependency.userSettings, notificationService: appDependency.notificationService)

            settingsViewController.onClose = { [weak self] in
                self?.navigationController.popToRootViewController(animated: true)
            }

            settingsViewController.onTap = { [weak self] id in
                self?.handleTap(id: id)
            }

            navigationController.pushViewController(settingsViewController, animated: true)

        case .premium:
            let premiumViewController = PremiumViewController()

            premiumViewController.onTerms = { [unowned self] in
                transition(to: .terms)
            }

            premiumViewController.onPrivacy = { [unowned self] in
                transition(to: .privacy)
            }

            premiumViewController.onBack = { [unowned self] in
                navigationController.popViewController(animated: true)
            }

            navigationController.pushViewController(premiumViewController, animated: true)

        case .terms:
            let termsViewController = TermsViewController()
            navigationController.pushViewController(termsViewController, animated: true)

        case .privacy:
            let privacyViewController = PrivacyViewController()
            navigationController.pushViewController(privacyViewController, animated: true)

        case .notification:
            break

        case .support:
            MailInteraction(message: "", delegate: self).follow()

        }

        self.page = page
    }

    // MARK: - Pages

    private func handleTap(id: SettingsItem.ID) {
        switch id {
        case .premium:
            transition(to: .premium)

        case .privacy:
            transition(to: .privacy)

        case .notification:
            break

        case .support:
            transition(to: .support)

        case .terms:
            transition(to: .terms)

        }
    }

}

extension SettingsCoordinator: MFMailComposeViewControllerDelegate {

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

}
