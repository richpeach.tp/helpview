import UIKit

enum Font {
    static let light = UIFont(name: "Roboto-Light", size: 17)!
    static let lightItalic = UIFont(name: "Roboto-LightItalic", size: 17)!
    static let regular = UIFont(name: "Roboto-Regular", size: 17)!
    static let regularItalic = UIFont(name: "Roboto-Italic", size: 17)!
    static let bold = UIFont(name: "Roboto-Bold", size: 17)!
    static let boldItalic = UIFont(name: "Roboto-BoldItalic", size: 17)!
    static let medium = UIFont(name: "Roboto-Medium", size: 17)!
    static let mediumItalic = UIFont(name: "Roboto-MediumItalic", size: 17)!
    static let caps = UIFont(name: "Intro-Bold-Caps", size: 18)!
    static let intro = UIFont(name: "Intro-Bold", size: 18)!
}

