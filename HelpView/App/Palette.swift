import UIKit

enum Palette {

    static let black = UIColor.black
    static let white = UIColor.white
    static let primary = UIColor.hex("#E04B44")
    static let primaryHighlighted = UIColor.hex("#D03B34")
    static let primaryBackground = UIColor.hex("#F5EDE5")
    static let rose = UIColor.hex("#F3E0D8")
    static let gray = UIColor.hex("#AAAAAA")
    static let shadow = UIColor.hex("#942B26")


}
