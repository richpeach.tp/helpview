import UIKit

final class FeedViewController: UIViewController {

    var onPremium: VoidHandler?

    var onOpenPostTap: ((FeedProtocol, Post) -> Void)?

    var onAddSpaceTap: VoidHandler?

    var onSplitTap: ((FeedProtocol, Post) -> Void)?

    var onHashtagTap: ((FeedProtocol, Post) -> Void)?

    var onSettingsTap: VoidHandler?

    var onCalendarTap: VoidHandler?

    //

    let feedService: FeedServiceProtocol

    let imageStorage: ImageStorageProtocol

    var spaceService: SpaceServiceProtocol

    let userSettings: UserSettingsProtocol

    private var factory = FeedFactory()

    private var imageSaver: ImageSaver?

    private var kind: Feed.Kind

    private var feed: FeedProtocol

    // MARK: - Init

    init(spaceService: SpaceServiceProtocol, feedService: FeedServiceProtocol, imageStorage: ImageStorageProtocol, userSettings: UserSettingsProtocol) {
        self.spaceService = spaceService
        self.feedService = feedService
        self.imageStorage = imageStorage
        self.userSettings = userSettings

        self.kind = .posts
        self.feed = feedService.feed(with: spaceService.current.id, andKind: self.kind)

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View Lifecycle

    private var feedView: FeedView {
        view as! FeedView
    }

    override func loadView() {
        view = FeedView(imageStorage: imageStorage)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Palette.primaryBackground

        self.spaceService.notifyOnCurrentSpaceChange { [weak self] in
            DispatchQueue.main.async {
                self?.factory.selectedPost = nil
                self?.reloadFeed()
            }
        }

        feedView.onPlaceholderTap = { [unowned self] in
            deselectCurrentPost()
            addNewPost()
        }

        feedView.onPostTap = { [unowned self] fileKey in
            guard let post = self.feed.posts.first(where: { $0.image.key == fileKey }) else { return }

            hideHeaderMenu()
            factory.selectedPost = post
            render()
        }

        feedView.onStoryTap = { [unowned self] fileKey in
            guard let post = self.feed.posts.first(where: { $0.image.key == fileKey }) else { return }

            hideHeaderMenu()
            factory.selectedStory = post
            render()
        }

        feedView.onPostMenuTap = { [unowned self] item in
            if kind == .posts {
                handlePostMenuTap(item: item)
            } else {
                handleStoryMenuTap(item: item)
            }
        }

        feedView.onKindTap = { [unowned self] selectedKind in
            kind = selectedKind
            deselectCurrentPost()
            reloadFeed()
        }

        feedView.onPostMenuCloseTap = { [unowned self] in
            deselectCurrentPost()
        }

        feedView.onOpenHeaderMenuTap = { [unowned self] in
            deselectCurrentPost()
            self.factory.showsHeaderMenu = true
            render()
        }

        feedView.onCloseHeaderMenuTap = { [unowned self] in
            deselectCurrentPost()
            self.factory.showsHeaderMenu = false
            render()
        }

        feedView.onHeaderMenuTap = { [unowned self] item in
            deselectCurrentPost()
            hideHeaderMenu()

            switch item {
            case .photo:
                addNewPost()

            case .story:
                addNewPost()

            case .space:
                onAddSpaceTap?()
            }
        }

        feedView.onReorder = { [unowned self] index, destination in
            feed.movePost(from: index, to: destination)
            render()
        }

        imageSaver = ImageSaver {
            AlertInteraction(title: "Successfully saved!", message: nil).follow()
        } onError: { _ in
            AlertInteraction(title: "Save error", message: "Failed to save an image to Gallery.").follow()
        }

        adjustNavigationBar()

        render()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.fill()
        reloadFeed()
    }

    // MARK: - Adjust

    private func adjustNavigationBar() {
        setupNavigationBar(backButtonAction: #selector(handleBackTap))
        navigationItem.titleView = UIImageView(imageNamed: "logo")
        navigationController?.navigationBar.fill()

        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: .plain, target: self, action: #selector(handleMenuTap))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "calendar"), style: .plain, target: self, action: #selector(handleCalendarTap))
    }

    // MARK: - Actions

    @objc
    private func handleMenuTap() {
        showMenu()
    }

    @objc
    private func handleCalendarTap() {
        onCalendarTap?()
    }

    @objc
    private func handleBackTap() {
        navigationController?.popToRootViewController(animated: true)
    }

    // MARK: - Render

    private func deselectCurrentPost() {
        factory.selectedPost = nil
        factory.selectedStory = nil
        render()
    }

    private func hideHeaderMenu() {
        factory.showsHeaderMenu = false
        render()
    }

    private func reloadFeed() {
        feed = feedService.feed(with: spaceService.current.id, andKind: self.kind)
        render()
    }

    private func render() {
        feedView.render(
            viewModel: .init(
                kind: kind,
                items: factory.generateItems(from: feed),
                showsPostMenu: factory.selectedPost != nil || factory.selectedStory != nil,
                showsHeaderMenu: factory.showsHeaderMenu
            )
        )
    }

    // MARK: - Menu

    private func handlePostMenuTap(item: FeedView.PostMenuView.Item) {
        guard let selectedPost = factory.selectedPost else { return }

        switch item {
        case .plan:
            deselectCurrentPost()
            onOpenPostTap?(feed, selectedPost)

        case .gallery:
            deselectCurrentPost()
            saveImageToGallery(from: selectedPost)

        case .hashtag:
            deselectCurrentPost()
            onHashtagTap?(feed, selectedPost)

        case .split:
            deselectCurrentPost()
            onSplitTap?(feed, selectedPost)

        case .delete:
            factory.selectedPost = nil
            feed.remove(selectedPost)
            render()
        }
    }

    private func handleStoryMenuTap(item: FeedView.PostMenuView.Item) {
        guard let selectedStory = factory.selectedStory else { return }

        switch item {
        case .delete:
            factory.selectedStory = nil
            feed.remove(selectedStory)
            render()

        default:
            break
        }
    }

    // MARK: - Add

    private func addPost(with photo: UIImage) {
        let key = Date().timeIntervalSince1970.description

        imageStorage.store(photo, forKey: key)
        feed.insert(Post(image: .init(key: key, size: photo.size)))

        render()
    }

    // MARK: - Saving

    private func saveImageToGallery(from post: Post) {
        guard let image = imageStorage.retrieveImage(forKey: post.image.key) else {
            AlertInteraction(title: "Save error", message: "Failed to save an image to Gallery.").follow()
            return
        }

        imageSaver?.writeToPhotoAlbum(image: image)
    }

}

extension FeedViewController {

    // MARK: - Menu

    private func showMenu() {
        let menuViewController = SideMenuViewController(spaceService: spaceService)

        menuViewController.onClose = {
            menuViewController.dismiss(animated: true)
        }

        menuViewController.onSpace = { [weak self] space in
            self?.spaceService.current = space
            menuViewController.dismiss(animated: true)
        }

        menuViewController.onAddSpace = {
            menuViewController.dismiss(animated: true) { [unowned self] in
                self.onAddSpaceTap?()
            }
        }

        menuViewController.onSettings = { [weak self] in
            menuViewController.dismiss(animated: true) { [unowned self] in
                self?.onSettingsTap?()
            }
        }

        present(menuViewController, animated: true)
    }

    // MARK: - Feed

    private func addNewPost() {
        let maxFreeCount = kind == .posts ? 12 : 5

        if feed.posts.count >= maxFreeCount && !userSettings.isPremium {
            onPremium?()
        } else {
            showGallery()
        }
    }

    private func showGallery() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.mediaTypes = ["public.image"]
        imagePicker.delegate = self
        present(imagePicker, animated: true)
    }

}

extension FeedViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else {
//            presentErrorAlert()
            // maybe iCloud?
            return
        }

        DispatchQueue.global(qos: .userInteractive).async {
            var fixedImage = image.fixOrientation()

            if let compressedData = fixedImage.compressed(), let image = UIImage(data: compressedData) {
                fixedImage = image
            }

            DispatchQueue.main.async {
                picker.dismiss(animated: true) { [weak self] in
                    self?.addPost(with: fixedImage)
                }
            }
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }

}


private enum Constants {

}
