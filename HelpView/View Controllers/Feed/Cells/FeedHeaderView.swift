import UIKit

extension FeedHeaderView {

    struct ViewModel {

        enum Kind {
            case posts
            case stories
        }

        let kind: Kind
        let isOpened: Bool

    }

}

class FeedHeaderView: UICollectionReusableView {

    var onPostsTap: VoidHandler?

    var onStoriesTap: VoidHandler?

    var onAddTap: VoidHandler?

    var onCloseTap: VoidHandler?

    // MARK: - Views

    lazy var postsButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(handlePostsTap), for: .touchUpInside)
        return button
    }()

    lazy var storiesButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(handleStoriesTap), for: .touchUpInside)
        return button
    }()

    lazy var dotView = UIView(backgroundColor: Palette.black)

    lazy var menuButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = Palette.primary
        button.setImage(UIImage(named: "add"), for: .normal)
        button.tintColor = Palette.white
        button.addTarget(self, action: #selector(handleMenuTap), for: .touchUpInside)
        return button
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        flex.direction(.row).justifyContent(.spaceBetween).alignItems(.center).paddingHorizontal(25).define { flex in
            flex.addItem().direction(.row).alignItems(.center).define { flex in
                flex.addItem(postsButton)
                flex.addItem(dotView).size(6).marginLeft(16)
                flex.addItem(storiesButton).marginLeft(16)
            }

            flex.addItem(menuButton).size(CGSize(side: 40))
        }
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        flex.layout()

        dotView.roundize()
        menuButton.roundize()
    }

    // MARK: - Actions

    @objc
    private func handlePostsTap() {
        onPostsTap?()
    }

    @objc
    private func handleStoriesTap() {
        onStoriesTap?()
    }

    @objc
    private func handleMenuTap() {
        if viewModel?.isOpened == true {
            onCloseTap?()
        } else {
            onAddTap?()
        }
    }

    // MARK: - Kind

    var viewModel: ViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }

            switch viewModel.kind {
            case .posts:
                postsButton.setAttributedTitle(String.attributedTitle("POST", isSelected: true, isCaps: true), for: .normal)
                storiesButton.setAttributedTitle(String.attributedTitle("STORIES", isSelected: false, isCaps: true), for: .normal)

            case .stories:
                postsButton.setAttributedTitle(String.attributedTitle("POST", isSelected: false, isCaps: true), for: .normal)
                storiesButton.setAttributedTitle(String.attributedTitle("STORIES", isSelected: true, isCaps: true), for: .normal)

            }

//            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut) {
                if viewModel.isOpened {
                    self.menuButton.setImage(UIImage(named: "close-header"), for: .normal)
//                    self.menuButton.transform = CGAffineTransform(rotationAngle: Degree(20).toRadians)
                } else {
                    self.menuButton.setImage(UIImage(named: "add"), for: .normal)
//                    self.menuButton.transform = CGAffineTransform(rotationAngle: Degree(10).toRadians)
                }
//            }
        }
    }

}

private enum Constants {

}
