import PinLayout

class StoryCell: UICollectionViewCell {

    // MARK: - Views

    let view = StoryView()

    var imageView: UIImageView {
        view.imageView
    }

    var selectionView: UIView {
        view.selectionView
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        contentView.addSubview(view)
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        view.pin.all()
    }

    // MARK: - Cell

    override func prepareForReuse() {
        super.prepareForReuse()

        imageView.image = nil
    }

}

private enum Constants {

}
