import UIKit

final class PostPlaceholderCell: UICollectionViewCell {

    let view = PostPlaceholderView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        contentView.addSubview(view)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        view.frame = bounds
    }

}
