import UIKit

class StoryView: UIView {

    // MARK: - Views

    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 15
        return imageView
    }()

    lazy var selectionView: UIView = {
        let view = UIView()
        view.layer.borderWidth = 4
        view.layer.cornerRadius = 15
        view.layer.borderColor = Palette.primary.cgColor
        return view
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        backgroundColor = Palette.rose
        addSubviews([imageView, selectionView])
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        imageView.pin.all()
        selectionView.pin.all(0)
    }

}

private enum Constants {

}
