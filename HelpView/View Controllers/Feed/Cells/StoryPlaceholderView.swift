import UIKit

class StoryPlaceholderView: UIView {

    // MARK: - Views

    let imageView = UIImageView(imageNamed: "placeholder-add")

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        backgroundColor = Palette.rose
        layer.borderWidth = 1
        layer.borderColor = Palette.white.cgColor
        layer.cornerRadius = 15

        addSubview(imageView)
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        imageView.pin
            .sizeToFit()
            .center()
    }

    // MARK: - Actions

}

private enum Constants {

}
