struct FeedFactory {

    var selectedPost: Post?

    var selectedStory: Post?

    var showsHeaderMenu: Bool = false

    func generateItems(from feed: FeedProtocol) -> [FeedView.Item] {
        switch feed.kind {
        case .stories:
            let postCount = feed.posts.count
            let placeholderCount = max(0, Constants.maxStoryPlaceholderCount - postCount)

            let placeholderItems = generatePlaceholders(count: placeholderCount, for: .stories)
            let postItems = feed.posts.map { post in
                FeedView.Item.story(key: post.image.key, isSelected: post == selectedStory)
            }

            return postItems + placeholderItems

        case .posts:
            let postCount = feed.posts.count
            let placeholderCount = max(0, Constants.maxPostPlaceholderCount - postCount)

            let placeholderItems = generatePlaceholders(count: placeholderCount, for: .posts)
            let postItems = feed.posts.map { post in
                FeedView.Item.post(key: post.image.key, isSelected: post == selectedPost)
            }

            return postItems + placeholderItems
        }
    }

    // MARK: - Placeholders

    private func generatePlaceholders(count: Int, for kind: Feed.Kind) -> [FeedView.Item] {
        var placeholders: [FeedView.Item] = []

        for _ in 0..<count {
            switch kind {
            case .posts:
                placeholders.append(.postPlaceholder)

            case .stories:
                placeholders.append(.storyPlaceholder)
            }
        }

        return placeholders
    }

}

private enum Constants {
    static let maxPostPlaceholderCount = 9
    static let maxStoryPlaceholderCount = 5
}
