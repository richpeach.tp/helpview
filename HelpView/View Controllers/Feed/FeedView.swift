import UIKit

extension FeedView {

    struct ViewModel {
        let items: [Item]
    }

    enum Item {
        case postPlaceholder
        case post
        case storyPlaceholder
        case story
    }

}

class FeedView: UIView {

    // MARK: - Views

    private let collectionView = UICollectionView.emptyCollection()

    private lazy var collectionViewAdapter = FeedViewAdapter(collectionView: collectionView)

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {

    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        collectionView.pin.all()
    }

    // MARK: - Actions

    // MARK: - Items

    func render(viewModel: ViewModel) {
        collectionViewAdapter.items = viewModel.items
    }

}

private enum Constants {

}
