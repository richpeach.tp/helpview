import UIKit

class ImageSaver: NSObject {

    let permissionsService = PhotoLibraryPermissionsService()

    let onSuccess: VoidHandler?

    let onError: ((Error) -> Void)?

    init(onSuccess: VoidHandler?, onError: ((Error) -> Void)?) {
        self.onSuccess = onSuccess
        self.onError = onError
        super.init()
    }

    func writeToPhotoAlbum(image: UIImage) {
        permissionsService.requestReadWriteAccess { [unowned self] in
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveError), nil)
        } onDeny: {
            AlertActionInteraction(
                title: "Sorry",
                message: "We need access to your photos. Switch this permission in your Settings.",
                action: UIAlertAction(title: "Settings", style: .default, handler: { _ in
                    SettingsInteraction().follow()
                })
            ).follow()
        }
    }

    @objc func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            onError?(error)
            return
        }

        onSuccess?()
    }

}
