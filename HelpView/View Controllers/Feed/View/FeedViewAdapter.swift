import UIKit

class FeedViewAdapter: NSObject {

    var onKindTap: ((Feed.Kind) -> Void)?

    var onPostTap: ((FileKey) -> Void)?

    var onStoryTap: ((FileKey) -> Void)?

    var onPlaceholderTap: VoidHandler?

    var onOpenHeaderMenuTap: VoidHandler?

    var onCloseHeaderMenuTap: VoidHandler?

    var onReorder: ((_ from: Int, _ to: Int) -> Void)?

    let collectionView: UICollectionView

    let imageStorage: ImageStorageProtocol

    var isDragging: Bool = false

    var isReordering: Bool = false

    init(collectionView: UICollectionView, imageStorage: ImageStorageProtocol) {
        self.collectionView = collectionView
        self.imageStorage = imageStorage

        super.init()

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
        collectionView.dragInteractionEnabled = true
        collectionView.backgroundColor = .clear
        collectionView.register(PostPlaceholderCell.self, forCellWithReuseIdentifier: PostPlaceholderCell.reuseIdentifier)
        collectionView.register(StoryPlaceholderCell.self, forCellWithReuseIdentifier: StoryPlaceholderCell.reuseIdentifier)
        collectionView.register(PostCell.self, forCellWithReuseIdentifier: PostCell.reuseIdentifier)
        collectionView.register(StoryCell.self, forCellWithReuseIdentifier: StoryCell.reuseIdentifier)
        collectionView.register(FeedHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: FeedHeaderView.reuseIdentifier)
    }

    // MARK: - View Model

    private(set) var items: [FeedView.Item] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    private(set) var kind: Feed.Kind? {
        didSet {
            (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing = kind == .stories ? Constants.spacingBetweenStories : 0
        }
    }

    private(set) var isHeaderMenuOpened: Bool = false

    func set(items: [FeedView.Item], kind: Feed.Kind, isHeaderMenuOpened: Bool) {
        self.kind = kind
        self.isHeaderMenuOpened = isHeaderMenuOpened
        self.items = items
    }

}

extension FeedViewAdapter: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.item]

        switch item {
        case .post(let key, let isSelected):
            let postCell = collectionView.dequeueReusableCell(withReuseIdentifier: PostCell.reuseIdentifier, for: indexPath) as! PostCell

            imageStorage.retrieveImage(forKey: key) { image in
                postCell.imageView.image = image
            }

            postCell.selectionView.isHidden = !isSelected

            return postCell

        case .postPlaceholder:
            return collectionView.dequeueReusableCell(withReuseIdentifier: PostPlaceholderCell.reuseIdentifier, for: indexPath)

        case .story(let key, let isSelected):
            let storyCell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryCell.reuseIdentifier, for: indexPath) as! StoryCell

            imageStorage.retrieveImage(forKey: key) { image in
                storyCell.imageView.image = image
            }

            storyCell.selectionView.isHidden = !isSelected

            return storyCell

        case .storyPlaceholder:
            return collectionView.dequeueReusableCell(withReuseIdentifier: StoryPlaceholderCell.reuseIdentifier, for: indexPath)

        }
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: FeedHeaderView.reuseIdentifier, for: indexPath) as! FeedHeaderView
            headerView.viewModel = .init(kind: self.kind == .posts ? .posts : .stories, isOpened: isHeaderMenuOpened)

            headerView.onStoriesTap = {
                if !self.isReordering {
                    self.onKindTap?(.stories)
                }
            }

            headerView.onPostsTap = {
                if !self.isReordering {
                    self.onKindTap?(.posts)
                }
            }

            headerView.onAddTap = {
                self.onOpenHeaderMenuTap?()
            }

            headerView.onCloseTap = {
                self.onCloseHeaderMenuTap?()
            }

            return headerView

        default:
            assert(false, "no")
        }
    }

}

extension FeedViewAdapter: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.item]

        switch item {
        case .postPlaceholder, .storyPlaceholder:
            onPlaceholderTap?()

        case .post(let fileKey, _):
            onPostTap?(fileKey)

        case .story(let fileKey, _):
            onStoryTap?(fileKey)

        }
    }

}

extension FeedViewAdapter: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = items[indexPath.item]

        switch item {
        case .postPlaceholder, .post:
            return squareCellSize

        case .storyPlaceholder, .story:
            return rectCellSize

        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: 68)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if kind == .stories {
            return UIEdgeInsets(top: 0, left: Constants.spacingBetweenStories, bottom: 0, right: Constants.spacingBetweenStories)
        }

        return .zero
    }

    private var squareCellSize: CGSize {
        let squareCellWidth = floor(collectionView.bounds.width / 3)
        return CGSize(side: squareCellWidth)
    }

    private var rectCellSize: CGSize {
        let rectCellWidth = floor((collectionView.bounds.width - Constants.spacingBetweenStories * 4) / 3)
        return CGSize(width: rectCellWidth, height: 16 / 9 * rectCellWidth)
    }

}

extension FeedViewAdapter: UICollectionViewDragDelegate {

    func collectionView(_ collectionView: UICollectionView, dragSessionWillBegin session: UIDragSession) {
        isDragging = true
    }

    func collectionView(_ collectionView: UICollectionView, dragSessionDidEnd session: UIDragSession) {
        isDragging = false
    }

    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let item = items[indexPath.item]

        guard kind == .posts, case .post = item, !isReordering, !isDragging else { return [] }

        let itemProvider = NSItemProvider(object: indexPath.description as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = item

        return [dragItem]
    }

}

extension FeedViewAdapter: UICollectionViewDropDelegate {

    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        guard collectionView.hasActiveDrag else { return .init(operation: .cancel) }

        // Calculating location in view
        let location = session.location(in: collectionView)
        var correctDestination: IndexPath?

        // Calculate index inside performUsingPresentationValues
        collectionView.performUsingPresentationValues {
            correctDestination = collectionView.indexPathForItem(at: location)
        }

        guard let destination = correctDestination else {
            return .init(operation: .cancel)
        }

        for i in (0...destination.item).reversed() {
            if i >= items.count { return .init(operation: .cancel) }

            switch items[i] {
            case .post:
                continue

            default:
                return .init(operation: .cancel)
            }
        }

        return .init(operation: .move, intent: .insertAtDestinationIndexPath)
    }

    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath: IndexPath

        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            destinationIndexPath = IndexPath(row: items.count, section: 0)
        }

        if coordinator.proposal.operation == .move {
            reorder(coordinator: coordinator, destinationIndexPath: destinationIndexPath)
        }
    }

    private func reorder(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath) {
        guard let item = coordinator.items.first,
              let sourceIndexPath = item.sourceIndexPath,
              let viewModel = item.dragItem.localObject as? FeedView.Item else {
            return
        }

        isReordering = true

        collectionView.performBatchUpdates({
            self.items.remove(at: sourceIndexPath.item)
            self.items.insert(viewModel, at: destinationIndexPath.item)

            collectionView.deleteItems(at: [sourceIndexPath])
            collectionView.insertItems(at: [destinationIndexPath])
        }, completion: { _ in
            self.onReorder?(sourceIndexPath.item, destinationIndexPath.item)
        })

        let drop = coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)

        drop.addCompletion { [weak self] _ in
            self?.isReordering = false // настоящий конец анимации происходит всегда здесь, а не при performBatchUpdates
        }
    }

}

private enum Constants {

    static let spacingBetweenStories: CGFloat = 12

}
