import UIKit

extension FeedView {

    struct ViewModel {
        let kind: Feed.Kind
        let items: [Item]
        let showsPostMenu: Bool
        let showsHeaderMenu: Bool
    }

    enum Item {
        case postPlaceholder
        case post(key: FileKey, isSelected: Bool)
        case storyPlaceholder
        case story(key: FileKey, isSelected: Bool)
    }

}

class FeedView: UIView {

    var onPlaceholderTap: VoidHandler? {
        didSet {
            collectionViewAdapter.onPlaceholderTap = onPlaceholderTap
        }
    }

    var onPostTap: ((FileKey) -> Void)? {
        didSet {
            collectionViewAdapter.onPostTap = onPostTap
        }
    }

    var onStoryTap: ((FileKey) -> Void)? {
        didSet {
            collectionViewAdapter.onStoryTap = onStoryTap
        }
    }

    var onPostMenuTap: ((PostMenuView.Item) -> Void)? {
        didSet {
            postMenuView.onTap = onPostMenuTap
        }
    }

    var onHeaderMenuTap: ((HeaderMenuView.Item) -> Void)? {
        didSet {
            headerMenuView.onTap = onHeaderMenuTap
        }
    }

    var onPostMenuCloseTap: VoidHandler? {
        didSet {
            postMenuView.onCloseTap = onPostMenuCloseTap
        }
    }

    var onKindTap: ((Feed.Kind) -> Void)? {
        didSet {
            collectionViewAdapter.onKindTap = onKindTap
        }
    }

    var onOpenHeaderMenuTap: VoidHandler? {
        didSet {
            collectionViewAdapter.onOpenHeaderMenuTap = onOpenHeaderMenuTap
        }
    }

    var onCloseHeaderMenuTap: VoidHandler? {
        didSet {
            collectionViewAdapter.onCloseHeaderMenuTap = onCloseHeaderMenuTap
        }
    }

    var onReorder: ((_ from: Int, _ to: Int) -> Void)? {
        didSet {
            collectionViewAdapter.onReorder = onReorder
        }
    }

    let imageStorage: ImageStorageProtocol

    // MARK: - Views

    private let collectionView = UICollectionView.emptyCollection()

    private lazy var collectionViewAdapter = FeedViewAdapter(collectionView: collectionView, imageStorage: imageStorage)

    private let postMenuView = PostMenuView()

    private let headerMenuView = HeaderMenuView()

    // MARK: - Init

    init(imageStorage: ImageStorageProtocol) {
        self.imageStorage = imageStorage

        super.init(frame: .zero)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Adjust

    private func adjustSubviews() {
        addSubviews([collectionView, postMenuView, headerMenuView])

        postMenuView.alpha = 0
        headerMenuView.alpha = 0
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        collectionView.pin.all()

        postMenuView.pin
            .width(240)
            .sizeToFit(.width)
            .bottom(safeAreaInsets.bottom + 20)
            .hCenter()

        headerMenuView.pin
            .width(168)
            .sizeToFit(.width)
            .top(66)
            .right(25)
    }

    // MARK: - Actions

    // MARK: - Items

    func render(viewModel: ViewModel) {
        let didChangeKind = postMenuView.kind != viewModel.kind

        collectionViewAdapter.set(
            items: viewModel.items,
            kind: viewModel.kind,
            isHeaderMenuOpened: viewModel.showsHeaderMenu
        )

        postMenuView.kind = viewModel.kind
        headerMenuView.kind = viewModel.kind

        UIView.animate(withDuration: didChangeKind ? 0 : 0.2, delay: 0, options: .curveEaseOut) {
            self.postMenuView.alpha = viewModel.showsPostMenu ? 1 : 0
            self.headerMenuView.alpha = viewModel.showsHeaderMenu ? 1 : 0
        }

        setNeedsLayout()
    }

}

private enum Constants {

}
