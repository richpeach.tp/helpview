import UIKit

extension FeedView {

    class PostMenuView: UIView {

        enum Item {
            case plan
            case split
            case hashtag
            case gallery
            case delete

            var title: String {
                switch self {
                case .plan:
                    return "plan / schedule post"
                case .split:
                    return "split photo"
                case .hashtag:
                    return "add hashtags"
                case .gallery:
                    return "SAVE TO GALlERY"
                case .delete:
                    return "delete photo"
                }
            }
        }

        var items: [Item] = []

        var kind: Feed.Kind? {
            didSet {
                if kind == .posts {
                    items = [.plan, .split, .hashtag, .gallery, .delete]
                } else {
                    items = [.delete]
                }

                adjustSubviews()
                setNeedsLayout()
            }
        }

        var onTap: ((Item) -> Void)?

        var onCloseTap: VoidHandler?

        // MARK: - Views

        var menuButtons: [UIButton] = []

        lazy var closeButton: UIButton = {
            let button = UIButton(type: .system)
            button.backgroundColor = Palette.primary
            button.setImage(UIImage(named: "close"), for: .normal)
            button.tintColor = Palette.white
            button.addTarget(self, action: #selector(handleCloseTap), for: .touchUpInside)
            return button
        }()

        // MARK: - Init

        override init(frame: CGRect) {
            super.init(frame: frame)

            adjustSubviews()
        }

        required init?(coder: NSCoder) {
            super.init(coder: coder)
        }

        // MARK: - Adjust

        private func adjustSubviews() {
            menuButtons.forEach { $0.removeFromSuperview() }
            menuButtons = generateMenuButtons()
            menuButtons.forEach { addSubview($0) }
            addSubview(closeButton)
        }

        private func generateMenuButtons() -> [UIButton] {
            items.enumerated().map { i, item in
                let button = UIButton(type: .system)
                button.setTitle(item.title.uppercased())
                button.backgroundColor = Palette.white
                button.titleLabel?.font = Constants.buttonFont
                button.setTitleColor(Palette.black, for: .normal)
                button.addTarget(self, action: #selector(handleMenuTap(button:)), for: .touchUpInside)
                button.tag = i
                return button
            }
        }

        // MARK: - Layout

        override func sizeThatFits(_ size: CGSize) -> CGSize {
            let buttonsCount = CGFloat(menuButtons.count)
            return CGSize(
                width: size.width,
                height: buttonsCount * Constants.buttonHeight
                    + (buttonsCount - 1) * Constants.buttonToButton
                    + Constants.buttonToButton + Constants.closeButtonSize.height
            )
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            menuButtons.enumerated().forEach { i, button in
                button.pin
                    .horizontally()
                    .top(CGFloat(i) * (Constants.buttonHeight + Constants.buttonToButton))
                    .height(Constants.buttonHeight)

                button.roundize()
            }

            closeButton.pin
                .size(Constants.closeButtonSize)
                .bottom()
                .hCenter()

            closeButton.roundize()
        }

        // MARK: - Actions

        @objc private func handleMenuTap(button: UIButton) {
            let item = items[button.tag]
            onTap?(item)
        }

        @objc private func handleCloseTap() {
            onCloseTap?()
        }

    }

}

private enum Constants {

    static let closeButtonSize = CGSize(side: 40)
    static let buttonHeight: CGFloat = 50
    static let buttonToButton: CGFloat = 10
    static let buttonFont = Font.medium.withSize(14)

}
