import UIKit

extension FeedView {

    class HeaderMenuView: UIView {

        enum Item {
            case photo
            case story
            case space

            var title: String {
                switch self {
                case .photo:
                    return "NEW PHOTO"

                case .story:
                    return "NEW STORY"

                case .space:
                    return "NEW SPACE"
                }
            }

            var image: UIImage? {
                switch self {
                case .photo:
                    return UIImage(named: "photo")

                case .story:
                    return UIImage(named: "story")

                case .space:
                    return UIImage(named: "space-black")
                }
            }
        }

        var items: [Item] = []

        var onTap: ((Item) -> Void)?

        var kind: Feed.Kind? {
            didSet {
                if kind == .posts {
                    self.items = [.photo, .space]
                } else {
                    self.items = [.story, .space]
                }

                adjustSubviews()
                setNeedsLayout()
            }
        }

        // MARK: - Views

        var menuButtons: [UIButton] = []

        // MARK: - Adjust

        private func adjustSubviews() {
            menuButtons.forEach { $0.removeFromSuperview() }
            menuButtons = generateMenuButtons()
            menuButtons.forEach { addSubview($0) }
        }

        private func generateMenuButtons() -> [UIButton] {
            items.enumerated().map { i, item in
                let button = UIButton(type: .system)
                button.setTitle(item.title.uppercased())
                button.setImage(item.image, for: .normal)
                button.tintColor = Palette.black
                button.backgroundColor = Palette.white
                button.titleLabel?.font = Constants.buttonFont
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
                button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
                button.setTitleColor(Palette.black, for: .normal)
                button.addTarget(self, action: #selector(handleMenuTap(button:)), for: .touchUpInside)
                button.tag = i
                return button
            }
        }

        // MARK: - Layout

        override func sizeThatFits(_ size: CGSize) -> CGSize {
            let buttonsCount = CGFloat(menuButtons.count)
            return CGSize(
                width: size.width,
                height: buttonsCount * Constants.buttonHeight
                    + (buttonsCount - 1) * Constants.buttonToButton
            )
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            menuButtons.enumerated().forEach { i, button in
                button.pin
                    .horizontally()
                    .top(CGFloat(i) * (Constants.buttonHeight + Constants.buttonToButton))
                    .height(Constants.buttonHeight)

                button.roundize()
            }
        }

        // MARK: - Actions

        @objc private func handleMenuTap(button: UIButton) {
            let item = items[button.tag]
            onTap?(item)
        }

    }

}

private enum Constants {

    static let buttonHeight: CGFloat = 50
    static let buttonToButton: CGFloat = 10
    static let buttonFont = Font.medium.withSize(14)

}
