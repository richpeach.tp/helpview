import UIKit

class AlertActionInteraction: Interaction {

    let title: String?
    let message: String?
    let action: UIAlertAction

    init(title: String? = nil, message: String? = nil, action: UIAlertAction) {
        self.title = title
        self.message = message
        self.action = action
    }

    func follow() {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(action)

        UIApplication.visibleViewContoller?.present(alert, animated: true)
    }

}

