import UIKit

class SettingsInteraction: Interaction {

    func follow() {
        let settingsUrl = URL(string: UIApplication.openSettingsURLString)

        if let url = settingsUrl {
            UIApplication.shared.open(url)
        }
    }

}
