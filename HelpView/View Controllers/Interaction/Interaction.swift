import Foundation

protocol Interaction {

    func follow()

}

