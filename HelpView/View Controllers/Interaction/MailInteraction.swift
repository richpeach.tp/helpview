import UIKit
import MessageUI

/// <#Description#>
class MailInteraction: NSObject, Interaction, MFMailComposeViewControllerDelegate {

    let mailto: String
    let message: String
    let delegate: MFMailComposeViewControllerDelegate

    /// <#Description#>
    /// - Parameters:
    ///   - mailto: <#mailto description#>
    ///   - message: <#message description#>
    ///   - delegate: <#delegate description#>
    init(mailto: String = "                                      ", message: String, delegate: MFMailComposeViewControllerDelegate) {
        self.mailto = mailto
        self.message = message
        self.delegate = delegate
    }

    func follow() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()

            mail.mailComposeDelegate = delegate
            mail.setToRecipients([mailto])

            var m = message
            m.append("\n\nVersion: \(App.version ?? "")\n")
            m.append("Model: \(Device.model)\n")
            m.append("iOS: \(Device.system)\n")
            m.append("Locale: \(Locale.current.identifier)")
            mail.setMessageBody(m, isHTML: false)

            UIApplication.visibleViewContoller?.present(mail, animated: true)
        } else {
            AlertInteraction(title: "Can't send email", message: nil).follow()
        }
    }

}
