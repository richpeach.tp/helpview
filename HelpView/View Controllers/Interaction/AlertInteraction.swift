import UIKit

class AlertInteraction: Interaction {

    let title: String?
    let message: String?

    init(title: String? = nil, message: String? = nil) {
        self.title = title
        self.message = message
    }

    func follow() {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))

        UIApplication.visibleViewContoller?.present(alert, animated: true)
    }

}
