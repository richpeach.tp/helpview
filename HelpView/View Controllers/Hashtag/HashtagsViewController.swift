import UIKit

final class HashtagsViewController: UIViewController {

    var onSave: VoidHandler?

    let post: Post

    init(post: Post) {
        self.post = post
        self.hashtags = post.hashtags.joined(separator: " ")

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView.emptyCollection()

        let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.scrollDirection = .vertical

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
        collectionView.register(InputCell.self, forCellWithReuseIdentifier: InputCell.reuseIdentifier)

        return collectionView
    }()

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Palette.primaryBackground
        view.addSubview(collectionView)

        navigationItem.rightBarButtonItem = .saveItem(target: self, action: #selector(handleSaveTap))
    }

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.pin.all()
    }

    // MARK: - Actions

    @objc
    private func handleSaveTap() {
        var caption = post.caption ?? ""

        post.hashtags.forEach { hashtag in
            if let range = caption.range(of: hashtag) {
               caption.removeSubrange(range)
            }
        }

        caption += "\n\n" + hashtags
        post.caption = caption.trimmingCharacters(in: .whitespacesAndNewlines)

        onSave?()
    }

    // MARK: - Content

    var hashtags: String

}

extension HashtagsViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: InputCell.reuseIdentifier, for: indexPath) as! InputCell

        cell.onInputChange = { [unowned self] cell in
            self.hashtags = cell.inputText
        }

        cell.viewModel = .init(title: nil, text: hashtags, placeholder: "Add Hashtags", isMultiline: true) { string, range, text in
            var str = string

            if text != ""
                && (string.count == 0
                || range.location > 0
                && (string as NSString).substring(with: NSRange(location: range.location - 1, length: 1)) == " ") {
                str = (str as NSString).replacingCharacters(in: range, with: "#" + text)
            } else {
                str = (str as NSString).replacingCharacters(in: range, with: text)
            }

            return str
        }

        return cell
    }

}

extension HashtagsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width - 22 - 22
        let height = InputCell.height(for: .init(title: nil, text: hashtags, placeholder: nil, isMultiline: true), maximumWidth: cellWidth)
        return CGSize(width: cellWidth, height: height)
    }

}

private enum Constants {

}
