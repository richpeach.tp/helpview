import UIKit

final class NewSpaceViewController: UIViewController {

    var onAddSpace: ((String) -> Void)?

    var spaceService: SpaceServiceProtocol

    init(spaceService: SpaceServiceProtocol) {
        self.spaceService = spaceService

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView.emptyCollection()

        let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.scrollDirection = .vertical

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
        collectionView.register(InputCell.self, forCellWithReuseIdentifier: InputCell.reuseIdentifier)

        return collectionView
    }()

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Palette.primaryBackground
        view.addSubview(collectionView)
    }

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.pin.all()
    }

    // MARK: - Actions

    @objc
    private func handleSaveTap() {
        if spaceService.spaces.first(where: { $0.title == name }) != nil {
            AlertInteraction(title: "Space name error", message: "Space with this name already exists.").follow()
            return
        }

        onAddSpace?(name)
    }

    // MARK: - Content

    var name: String = "" {
        didSet {
            if name.isEmpty {
                navigationItem.rightBarButtonItem = nil
            } else {
                navigationItem.rightBarButtonItem = .saveItem(target: self, action: #selector(handleSaveTap))
            }
        }
    }

}

extension NewSpaceViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: InputCell.reuseIdentifier, for: indexPath) as! InputCell

        cell.onInputChange = { [unowned self] cell in
            self.name = cell.inputText
        }

        cell.viewModel = .init(title: nil, text: nil, placeholder: "Add Space name", isMultiline: false)

        return cell
    }

}

extension NewSpaceViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width - 22 - 22
        let height = InputCell.height(for: .init(title: nil, text: name, placeholder: nil, isMultiline: false), maximumWidth: cellWidth)
        return CGSize(width: cellWidth, height: height)
    }

}

private enum Constants {

}
