import UIKit

extension OnboardingViewController {

    static var viewModels: [OnboardingPageView.ViewModel] {
        [
            .init(
                titleImage: UIImage(named: "onboarding-title-1")!,
                subtitleImage: UIImage(named: "onboarding-subtitle-1")!,
                text: "PREVIEW your feed design\nbefore publication on\ninstagram".uppercased(),
                cornerImage: UIImage(named: "onboarding-corner-1")!,
                page: 0
            ),
            .init(
                titleImage: UIImage(named: "onboarding-title-2")!,
                subtitleImage: UIImage(named: "onboarding-subtitle-2")!,
                text: "Plan every post\nof your feed and\nschedule it".uppercased(),
                cornerImage: UIImage(named: "onboarding-corner-2")!,
                page: 1
            ),
            .init(
                titleImage: UIImage(named: "onboarding-title-3")!,
                subtitleImage: UIImage(named: "onboarding-subtitle-3")!,
                text: "SPLIT YOUR PHOTOS TO\nCREATE INCREDIBLE VIEW\nOF YOUR FEED".uppercased(),
                cornerImage: UIImage(named: "onboarding-corner-3")!,
                page: 2
            )
        ]
    }

}
