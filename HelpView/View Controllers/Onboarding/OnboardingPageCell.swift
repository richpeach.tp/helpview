import UIKit

final class OnboardingPageCell: UICollectionViewCell {

    var onFinish: VoidHandler? {
        didSet {
            pageView.onFinish = onFinish
        }
    }

    let pageView = OnboardingPageView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        contentView.addSubview(pageView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var viewModel: OnboardingPageView.ViewModel? {
        didSet {
            pageView.viewModel = viewModel
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        pageView.frame = bounds
    }

}
