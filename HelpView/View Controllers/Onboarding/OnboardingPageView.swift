import PinLayout

extension OnboardingPageView {

    struct ViewModel {
        let titleImage: UIImage
        let subtitleImage: UIImage
        let text: String
        let cornerImage: UIImage
        let page: Int
    }

}

class OnboardingPageView: UIView {

    var onFinish: VoidHandler?

    // MARK: - Views

    private let titleImageView = UIImageView()

    private let subtitleImageView = UIImageView()

    private let textLabel = UILabel(
        text: nil,
        textColor: Palette.white,
        font: Font.medium.withSize(14)
    )

    private let cornerImageView = UIImageView()

    private let dotsView = DotsView()

    private var nextButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "next"), for: .normal)
        button.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
        return button
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubviews([
            titleImageView,
            subtitleImageView,
            textLabel,
            cornerImageView,
            dotsView,
            nextButton
        ])

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        titleImageView.contentMode = .scaleAspectFit
        subtitleImageView.contentMode = .scaleAspectFit
        dotsView.count = 3

        textLabel.numberOfLines = 0
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        let insets = Constants.insets
        let maximumWidth = bounds.width - insets.horizontal

        titleImageView.pin
            .left(insets.left)
            .top(insets.top + safeAreaInsets.top)
            .sizeToFit()
            .maxWidth(maximumWidth)

        subtitleImageView.pin
            .left(insets.left)
            .below(of: titleImageView)
            .marginTop(Constants.titleToSubtitle)
            .sizeToFit()
            .maxWidth(maximumWidth)

        textLabel.pin
            .left(insets.left)
            .below(of: subtitleImageView)
            .marginTop(Constants.subtitleToText)
            .sizeToFit()
            .maxWidth(maximumWidth)

        let size = cornerImageView.image!.size
        let aspectRatio = size.width / size.height

        cornerImageView.pin
            .height(45%)
            .aspectRatio(aspectRatio)
            .bottom()
            .right()

        dotsView.pin
            .left(insets.left)
            .below(of: textLabel)
            .marginTop(Constants.textToDots)
            .sizeToFit()

        nextButton.pin
            .sizeToFit()
            .after(of: dotsView, aligned: .center)
            .marginLeft(5)
    }

    // MARK: - Actions

    @objc
    private func handleNext() {
        onFinish?()
    }

    // MARK: - Model

    var viewModel: ViewModel? {
        didSet {
            titleImageView.image = viewModel?.titleImage
            subtitleImageView.image = viewModel?.subtitleImage
            textLabel.attributedText = viewModel?.text.withAttributes(font: textLabel.font, color: textLabel.textColor, lineSpacing: 6)
            cornerImageView.image = viewModel?.cornerImage
            dotsView.selectedIndex = viewModel?.page ?? 0

            setNeedsLayout()
        }
    }

}

private enum Constants {
    static let insets = UIEdgeInsets(top: 94, left: 25, bottom: 0, right: 25)
    static let titleToSubtitle: CGFloat = 25
    static let subtitleToText: CGFloat = 35
    static let textToDots: CGFloat = 45
}
