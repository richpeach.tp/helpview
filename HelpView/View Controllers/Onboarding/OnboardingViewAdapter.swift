import UIKit

final class OnboardingViewAdapter: NSObject {

    var onNextPage: VoidHandler?

    var viewModels: [OnboardingPageView.ViewModel] = []

    let collectionView: UICollectionView

    var currentPage: Int {
        get {
            let width = collectionView.frame.width
            return Int((collectionView.contentOffset.x + (0.5 * width)) / width)
        }
        set {
            collectionView.scrollToItem(at: IndexPath(item: newValue, section: 0), at: .centeredHorizontally, animated: true)
        }
    }

    init(collectionView: UICollectionView) {
        self.collectionView = collectionView

        super.init()

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = .horizontal
        collectionView.register(OnboardingPageCell.self, forCellWithReuseIdentifier: OnboardingPageCell.reuseIdentifier)
    }
}

extension OnboardingViewAdapter: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OnboardingPageCell.reuseIdentifier, for: indexPath) as! OnboardingPageCell
        cell.viewModel = viewModels[indexPath.item]
        cell.onFinish = onNextPage
        return cell
    }

}

extension OnboardingViewAdapter: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.bounds.size
    }

}
