import UIKit

final class OnboardingViewController: UIViewController {

    var onFinish: VoidHandler?

    // MARK: - Views

    private let collectionView = UICollectionView.emptyCollection()

    private lazy var collectionViewAdapter = OnboardingViewAdapter(collectionView: collectionView)

    private var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "close"), for: .normal)
        button.tintColor = Palette.white
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        return button
    }()

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Palette.primary
        view.addSubviews([collectionView, closeButton])

        collectionViewAdapter.viewModels = Self.viewModels

        collectionViewAdapter.onNextPage = { [unowned self] in
            self.handleNext()
        }
    }

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.pin.all()

        closeButton.pin
            .sizeToFit()
            .right(5)
            .top(view.safeAreaInsets.top + 5)
    }

    // MARK: - Actions

    @objc private func handleNext() {
        let currentPage = collectionViewAdapter.currentPage

        if currentPage == collectionViewAdapter.viewModels.count - 1 {
            onFinish?()
        } else {
            collectionViewAdapter.currentPage = currentPage + 1
        }
    }

    @objc private func handleClose() {
        onFinish?()
    }

}

private enum Constants {

}
