import UIKit

final class SideMenuTransitionAnimator: NSObject {

    enum TransitionMode {
        case present
        case dismiss
    }

    let mode: TransitionMode

    init(mode: TransitionMode) {
        self.mode = mode

        super.init()
    }

}

extension SideMenuTransitionAnimator: UIViewControllerAnimatedTransitioning {

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let duration = transitionDuration(using: transitionContext)

        switch mode {
        case .present:
            guard let toViewController = transitionContext.viewController(forKey: .to) as? SideMenuViewController,
                  let toView = toViewController.view else { return }

            toView.setShadow(withColor: Palette.shadow, radius: 30, opacity: 0.5, offset: .init(width: 10, height: 0))

            let fullFrame = transitionContext.finalFrame(for: toViewController)
            var frame = fullFrame
            frame.size.width = Constants.menuWidth
            frame.origin.x = -Constants.menuWidth
            toView.frame = frame

            containerView.addSubview(toView)

            UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.5, options: .curveEaseOut) {
                frame.origin.x = 0
                toView.frame = frame
            } completion: { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }

        case .dismiss:
            guard let fromViewController = transitionContext.viewController(forKey: .from) as? SideMenuViewController,
                  let fromView = fromViewController.view else { return }

            var frame = fromView.frame
            frame.origin.x = -Constants.menuWidth

            UIView.animate(
                withDuration: duration,
                delay: 0,
                options: .curveEaseOut
            ) {
                fromView.frame = frame
            } completion: { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
        }
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        mode == .present ? 0.4 : 0.15
    }

}

private enum Constants {

    static let menuWidth: CGFloat = 320

}
