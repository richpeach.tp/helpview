import UIKit

final class SideMenuViewController: UIViewController {

    var onClose: VoidHandler?

    var onSpace: ((Space) -> Void)?

    var onAddSpace: VoidHandler?

    var onSettings: VoidHandler?

    let spaceService: SpaceServiceProtocol

    init(spaceService: SpaceServiceProtocol) {
        self.spaceService = spaceService

        super.init(nibName: nil, bundle: nil)

        self.transitioningDelegate = self
        self.modalPresentationStyle = .overFullScreen
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views

    private let collectionView = UICollectionView.emptyCollection()

    private lazy var collectionViewAdapter = SideMenuViewAdapter(collectionView: collectionView)

    private var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "small-close"), for: .normal)
        button.tintColor = Palette.white
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        return button
    }()

    private var settingsView = SideMenuRowView()

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Palette.primary
        view.addSubviews([collectionView, closeButton, settingsView])

        fillViewModels()

        collectionViewAdapter.onTap = { [weak self] viewModel in
            switch viewModel.kind {
            case .add:
                self?.onAddSpace?()

            case .space(let id):
                self?.spaceService.spaces.first(where: { $0.id == id }).map {
                    self?.onSpace?($0)
                }

            default:
                break
            }
        }

        let tap = UITapGestureRecognizer(target: self, action: #selector(handleSettingsTap))
        settingsView.addGestureRecognizer(tap)
    }

    // MARK: - Setup

    private func fillViewModels() {
        let spacesViewModels = spaceService.spaces.map {
            SideMenuRowView.ViewModel(
                kind: .space($0.id),
                icon: UIImage(named: "space")!,
                title: $0.title,
                showDot: spaceService.current == $0
            )
        }

        collectionViewAdapter.viewModels = spacesViewModels + [
            .init(kind: .add, icon: UIImage(named: "add-space")!, title: "Create new Space", showDot: false)
        ]

        settingsView.viewModel = .init(kind: nil, icon: UIImage(named: "settings")!, title: "Settings", showDot: false)
    }

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        closeButton.pin
            .sizeToFit()
            .left(16)
            .top(view.safeAreaInsets.top)

        settingsView.pin
            .bottom(view.safeAreaInsets.bottom + 30)
            .horizontally()
            .height(SideMenuRowView.height)

        collectionView.pin
            .below(of: closeButton)
            .above(of: settingsView)
            .horizontally()
    }

    // MARK: - Actions

    @objc
    private func handleClose() {
        onClose?()
    }

    @objc
    private func handleSettingsTap() {
        onSettings?()
    }

}

extension SideMenuViewController: UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        SideMenuTransitionAnimator(mode: .present)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        SideMenuTransitionAnimator(mode: .dismiss)
    }

}


private enum Constants {

}
