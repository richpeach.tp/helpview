import UIKit

final class SideMenuViewAdapter: NSObject {

    var viewModels: [SideMenuRowView.ViewModel] = []

    var onTap: ((SideMenuRowView.ViewModel) -> Void)?

    let collectionView: UICollectionView

    init(collectionView: UICollectionView) {
        self.collectionView = collectionView

        super.init()

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.delaysContentTouches = false
        collectionView.register(SideMenuCell.self, forCellWithReuseIdentifier: SideMenuCell.reuseIdentifier)
    }
}

extension SideMenuViewAdapter: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SideMenuCell.reuseIdentifier, for: indexPath) as! SideMenuCell
        cell.viewModel = viewModels[indexPath.item]
        return cell
    }

}

extension SideMenuViewAdapter: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        onTap?(viewModels[indexPath.item])
    }

}

extension SideMenuViewAdapter: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: SideMenuCell.height)
    }

}
