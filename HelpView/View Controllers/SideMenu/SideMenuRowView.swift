import UIKit

extension SideMenuRowView {

    struct ViewModel {
        enum Kind {
            case space(Space.ID)
            case add
        }

        let kind: Kind?
        let icon: UIImage
        let title: String
        let showDot: Bool
    }

}

class SideMenuRowView: UIControl {

    // MARK: - Views

    private let imageView = UIImageView()

    private let titleLabel = UILabel(text: nil, textColor: Palette.white, font: Font.regular.withSize(18))

    private let dotView = UIView(backgroundColor: Palette.white)

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        addSubviews([imageView, titleLabel, dotView])
    }

    // MARK: - Layout

    static var height: CGFloat {
        Constants.insets.vertical + Constants.imageSize.height
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let insets = Constants.insets

        imageView.pin
            .size(Constants.imageSize)
            .left(insets.left)
            .vCenter()

        titleLabel.pin
            .sizeToFit()
            .after(of: imageView)
            .marginLeft(Constants.iconToTitle)
            .vCenter()

        dotView.pin
            .size(Constants.dotSize)
            .after(of: titleLabel)
            .marginLeft(Constants.titleToDot)
            .vCenter()

        dotView.roundize()
    }

    // MARK: - Cell

    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? Palette.primaryHighlighted : nil
        }
    }

    // MARK: - Model

    var viewModel: ViewModel? {
        didSet {
            imageView.image = viewModel?.icon
            titleLabel.text = viewModel?.title
            dotView.isHidden = !(viewModel?.showDot == true)
            setNeedsLayout()
        }
    }

}

private enum Constants {
    static let insets = UIEdgeInsets(top: 16, left: 32, bottom: 16, right: 32)

    static let iconToTitle: CGFloat = 16
    static let titleToDot: CGFloat = 16

    static let imageSize = CGSize(side: 30)
    static let dotSize = CGSize(side: 8)
}
