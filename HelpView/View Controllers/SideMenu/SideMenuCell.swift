import UIKit

final class SideMenuCell: UICollectionViewCell {

    let view = SideMenuRowView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        view.isUserInteractionEnabled = false
        contentView.addSubview(view)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var viewModel: SideMenuRowView.ViewModel? {
        didSet {
            view.viewModel = viewModel
        }
    }

    override var isHighlighted: Bool {
        didSet {
            view.isHighlighted = isHighlighted
        }
    }

    static var height: CGFloat {
        SideMenuRowView.height
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        view.frame = bounds
    }

}
