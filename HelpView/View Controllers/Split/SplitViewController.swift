import UIKit

final class SplitViewController: UIViewController {

    let feed: FeedProtocol

    let imageStorage: ImageStorageProtocol

    let post: Post

    var onSave: VoidHandler?

    let image: UIImage

    let imageHelper: SplitImageHelper

    init(feed: FeedProtocol, post: Post, image: UIImage, imageStorage: ImageStorageProtocol) {
        self.feed = feed
        self.post = post
        self.imageStorage = imageStorage
        self.image = image
        self.imageHelper = SplitImageHelper(image: image)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views

    private lazy var rowsControl: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        return stackView
    }()

    private var imageViews: [UIImageView] = []

    private var numberOfRows: Int = 1 {
        didSet {
            let bigImage = imageHelper.cropToBounds(image: image)
            images = imageHelper.images(from: bigImage, rows: numberOfRows)

            imageViews.forEach { $0.removeFromSuperview() }
            imageViews = generateImageViews()
            imageViews.forEach { view.addSubview($0) }

            rowsControl.arrangedSubviews.forEach {
                rowsControl.removeArrangedSubview($0)
                $0.removeFromSuperview()
            }
            generateRowButtons().forEach { rowsControl.addArrangedSubview($0) }
        }
    }

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = .saveItem(target: self, action: #selector(handleSaveTap))
        view.backgroundColor = Palette.primaryBackground

//        imageViews = generateImageViews()
//        imageViews.forEach { view.addSubview($0) }

        view.addSubview(rowsControl)

        self.numberOfRows = 1
    }

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        rowsControl.pin.height(60).horizontally(Constants.insets.left)

        let imageViewSide = (view.bounds.width
            - Constants.insets.horizontal
            - Constants.imageToImage * 2)
            / 3

        imageViews.enumerated().forEach { i, imageView in
            let row = CGFloat(Int(i / 3))
            let column = CGFloat(i % 3)

            imageView.pin
                .width(imageViewSide)
                .height(imageViewSide)
                .top(row * (imageViewSide + Constants.imageToImage) + rowsControl.bounds.height)
                .left(column * (imageViewSide + Constants.imageToImage) + Constants.insets.left)
        }
    }

    // MARK: - Actions

    @objc
    private func handleButtonTap(button: UIButton) {
        numberOfRows = button.tag
    }

    @objc private func handleSaveTap() {
        replaceImageWithSplits()
    }

    // MARK: - Images

    private var images: [UIImage] = []

    private func replaceImageWithSplits() {
        guard let index = feed.posts.firstIndex(of: post) else { return }

        let posts: [Post] = images.map { image in
            let key = Date().timeIntervalSince1970.description
            imageStorage.store(image, forKey: key)

            let newPost = Post(image: Post.Image(key: key, size: image.size))
            newPost.caption = post.caption
            newPost.publishDate = post.publishDate
            return newPost
        }

        feed.insert(posts, at: index)
        feed.remove(post)
        onSave?()
    }

    // MARK: - Helpers

    private func generateImageViews() -> [UIImageView] {
        var imageViews: [UIImageView] = []
        let count = numberOfRows * 3

        for i in 0..<count {
            let imageView = UIImageView(image: images[i])
            imageViews.append(imageView)
        }

        return imageViews
    }

    private func generateRowButtons() -> [UIButton] {
        var buttons: [UIButton] = []

        for i in 1...3 {
            let button = UIButton(type: .custom)
            button.setAttributedTitle(
                String.attributedTitle("3 x \(i)", isSelected: i == numberOfRows, isCaps: false),
                for: .normal
            )
            button.tag = i
            button.addTarget(self, action: #selector(handleButtonTap(button:)), for: .touchUpInside)
            buttons.append(button)
        }

        return buttons
    }

}

private enum Constants {
    static let imageToImage: CGFloat = 2
    static let insets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
}
