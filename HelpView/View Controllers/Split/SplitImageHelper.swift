import UIKit

class SplitImageHelper {

    let image: UIImage

    init(image: UIImage) {
        self.image = image
    }

    func cropToBounds(image: UIImage) -> UIImage {
        let cgimage = image.cgImage!
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        let contextSize: CGSize = contextImage.size
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat //= CGFloat(width)
        var cgheight: CGFloat //= CGFloat(height)

        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }

        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)

        // Create bitmap image from context using the rect
        let imageRef: CGImage = cgimage.cropping(to: rect)!

        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)

        return image
    }

    func images(from image: UIImage, rows: Int) -> [UIImage] {
        var images: [UIImage] = []
        let cgimage = image.cgImage!
        let fullSize: CGSize = image.size
        let side = fullSize.width / 3

        for row in 0..<rows {
            for column in 0..<3 {
                let rect = CGRect(x: CGFloat(column) * side, y: CGFloat(row) * side, width: side, height: side)
                let imageRef: CGImage = cgimage.cropping(to: rect)!
                let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
                images.append(image)
            }
        }

        return images
    }

}
