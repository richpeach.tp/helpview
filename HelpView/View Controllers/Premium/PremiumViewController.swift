import UIKit

final class PremiumViewController: UIViewController {

    var onBack: VoidHandler?

    var onPrivacy: VoidHandler? {
        didSet {
            premiumView.onPrivacy = onPrivacy
        }
    }

    var onTerms: VoidHandler? {
        didSet {
            premiumView.onTerms = onTerms
        }
    }

    // MARK: - Views

    var premiumView: PremiumView {
        view as! PremiumView
    }

    // MARK: - View Lifecycle

    override func loadView() {
        self.view = PremiumView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupNavigationBar(backButtonAction: #selector(handleBackTap))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.makeTransparent()
    }

    // MARK: - Setup

    private func setupView() {
        premiumView.onContinue = { subscriptionId in
            print(subscriptionId)
            // TODO: ЗДЕСЬ ВЫЗЫВАТЬ ЭПЛОВСКИЙ ЭКРАНЧИК ПОКУПКИ
        }

        premiumView.onRestore = {}
    }

    // MARK: - Actions

    @objc
    private func handleBackTap() {
        onBack?()
    }

}

private enum Constants {

}
