import UIKit

typealias SubscriptionID = String

extension PremiumSubscriptionsView {

    class Option {

        enum Duration: String {
            case week
            case month
            case year
        }

        let id: SubscriptionID // id of subscription
        let duration: Duration
        let price: Double
        var isSelected: Bool

        init(id: SubscriptionID, duration: Duration, price: Double, isSelected: Bool) {
            self.id = id
            self.duration = duration
            self.price = price
            self.isSelected = isSelected
        }

    }

}

class PremiumSubscriptionsView: UIView {

    var options: [Option] = [] {
        didSet {
            fill()
        }
    }

    var selectedOption: Option? {
        options.first { $0.isSelected }
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup

    private func adjustSubviews() {
        addSubview(containerView)
    }

    private func setupFlexLayout() {
        containerView.flex.direction(.column).define { flex in
            optionViews.forEach {
                flex
                .addItem($0)
                .grow(1)
                .marginBottom(16)
                .height(72)
            }
        }
    }

    // MARK: - Views

    private var optionViews: [SubscriptionOptionView] = []

    lazy var containerView = UIView()

    // MARK: - Fill

    private func fill() {
        optionViews.forEach { $0.removeFromSuperview() }
        optionViews = []

        for option in options {
            let optionView = self.optionView(with: option)
            optionViews.append(optionView)
            containerView.addSubview(optionView)
        }

        setupFlexLayout()

        setNeedsLayout()
    }

    // MARK: - Render

    private func optionView(with option: Option) -> SubscriptionOptionView {
        let optionView = SubscriptionOptionView(frame: .zero)

        optionView.onTap = { [weak self] in
            self?.options.forEach { $0.isSelected = false }
            option.isSelected = true

            self?.rerender()
        }

        optionView.model = option
        return optionView
    }

    private func rerender() {
        optionViews.forEach { $0.render() }
    }

    // MARK: - Layout

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        CGSize(
            width: size.width,
            height: CGFloat(options.count * 72 + (options.count - 1) * 16)
        )
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        containerView.pin.all()

        containerView.flex.layout()
    }

}
