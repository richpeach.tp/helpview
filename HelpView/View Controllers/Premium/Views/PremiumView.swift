import UIKit
import FlexLayout

class PremiumView: UIView {

    var onContinue: ((SubscriptionID) -> Void)?

    var onRestore: VoidHandler?

    var onPrivacy: VoidHandler?

    var onTerms: VoidHandler?

    // MARK: - Views

    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView(imageNamed: "premium-background")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var scrollView = UIScrollView()

    private lazy var contentView = UIView()

    private lazy var titleImageView: UIImageView = {
        let imageView = UIImageView(imageNamed: "premium-title")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var infoTable: UIView = {
        let view = UIView()
        view.flex.direction(.column).define { flex in
            Constants.infoRows.forEach { row in
                flex.addItem(PremiumInfoRow(text: row)).marginBottom(12)
            }
        }
        return view
    }()

    private lazy var bestValueLabel = UILabel(text: "BEST VALUE", textColor: Palette.white, font: Font.bold.withSize(10))

    private lazy var subscriptionsView = PremiumSubscriptionsView()

    private lazy var continueButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("CONTINUE")
        button.setTitleColor(Palette.black, for: .normal)
        button.titleLabel?.font = Font.bold.withSize(18)
        button.backgroundColor = Palette.white
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(handleContinueTap), for: .touchUpInside)
        return button
    }()

    private lazy var restoreButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("RESTORE PURCHASE")
        button.setImage(UIImage(named: "restore"), for: .normal)
        button.titleLabel?.font = Font.medium.withSize(12)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        button.tintColor = Palette.white
        button.addTarget(self, action: #selector(handleRestoreTap), for: .touchUpInside)
        return button
    }()

    private lazy var linksTextView: UITextView = {
        let textView = UITextView()
        let text = "See our privacy policy and terms of use"
        let mutableAttributedString = NSMutableAttributedString(
            string: text,
            attributes: [.foregroundColor: Palette.white, .font: Font.regular.withSize(14)]
        )
        mutableAttributedString.addAttributes(
            [.link: Constants.privacyLink],
            range: (text as NSString).range(of: "privacy policy")
        )
        mutableAttributedString.addAttributes(
            [.link: Constants.termsLink],
            range: (text as NSString).range(of: "terms of use")
        )
        textView.linkTextAttributes = [.foregroundColor: Palette.white]
        textView.tintColor = Palette.white
        textView.attributedText = mutableAttributedString
        textView.isEditable = false
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.textContainerInset = .zero
        textView.textContainer.lineFragmentPadding = 0
        textView.delegate = self
        return textView
    }()

    private lazy var informationLabel = UILabel(text: Constants.informationText, textColor: Palette.white, font: Font.regular.withSize(12))

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        backgroundColor = Palette.primary
        contentView.backgroundColor = Palette.primary

        addSubviews([backgroundImageView, scrollView])

        scrollView.addSubview(contentView)

        scrollView.contentInset = UIEdgeInsets(top: 160, left: 0, bottom: 20, right: 0)
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.delaysContentTouches = false
        scrollView.alwaysBounceVertical = true
        scrollView.backgroundColor = .clear

        contentView.addSubviews([
            titleImageView,
            infoTable,
            bestValueLabel,
            subscriptionsView,
            continueButton,
            restoreButton,
            linksTextView,
            informationLabel
        ])

        subscriptionsView.options = [
            .init(id: "month", duration: .month, price: 10.99, isSelected: true),
            .init(id: "year", duration: .year, price: 30.99, isSelected: false),
        ]

        bestValueLabel.backgroundColor = Palette.black
        bestValueLabel.textAlignment = .center

        informationLabel.numberOfLines = 0
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        let insets = Constants.insets

        let aspectFit = backgroundImageView.image!.size.widthToHeightRatio
        backgroundImageView.pin
            .top()
            .horizontally()
            .aspectRatio(aspectFit)

        scrollView.pin
            .horizontally()
            .top()
            .bottom()

        contentView.pin
            .horizontally()
            .top()

        titleImageView.pin
            .top(insets.top)
            .horizontally(Constants.horizontalMargin)

        layoutInfoTable()

        bestValueLabel.pin
            .below(of: infoTable)
            .width(72)
            .height(32)
            .marginTop(24)
            .left(42)

        bestValueLabel.roundize(cornerRadius: 9, clips: true)

        subscriptionsView.pin
            .below(of: infoTable)
            .marginTop(50)
            .horizontally(Constants.horizontalMargin)
            .sizeToFit(.width)

        continueButton.pin
            .below(of: subscriptionsView)
            .marginTop(16)
            .height(50)
            .horizontally(Constants.horizontalMargin)

        restoreButton.pin
            .below(of: continueButton)
            .marginTop(18)
            .sizeToFit()
            .hCenter()

        linksTextView.pin
            .below(of: restoreButton)
            .marginTop(20)
            .sizeToFit()
            .hCenter()

        informationLabel.pin
            .below(of: linksTextView)
            .marginTop(16)
            .horizontally(Constants.horizontalMargin)
            .sizeToFit(.width)

        contentView.pin
            .height(
                max(
                    informationLabel.frame.maxY + insets.bottom + safeAreaInsets.bottom,
                    bounds.height
                ) + 300
            )

        scrollView.contentSize = CGSize(width: contentView.bounds.width, height: contentView.bounds.height - 300)

        contentView.roundize(cornerRadius: 40, clips: true)
    }

    private func layoutInfoTable() {
        let width = infoTable.subviews
            .map { $0.sizeThatFits(
                CGSize(width: bounds.width, height: .greatestFiniteMagnitude)
            ) }
            .max { f, s in
            f.width < s.width
        }?.width

        width.map { width in
            infoTable.pin
                .below(of: titleImageView)
                .marginTop(16)
                .width(width)
                .hCenter()

            infoTable.flex.layout(mode: .adjustHeight)
        }

    }

    // MARK: - Actions

    @objc
    private func handleContinueTap() {
        selectedSubscriptionId.map { onContinue?($0) }
    }

    @objc
    private func handleRestoreTap() {
        onRestore?()
    }

    // MARK: - Subscriptions

    private var selectedSubscriptionId: SubscriptionID? {
        subscriptionsView.selectedOption?.id
    }

}

extension PremiumView: UITextViewDelegate {

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == Constants.privacyLink {
            onPrivacy?()
        } else if URL.absoluteString == Constants.termsLink {
            onTerms?()
        }

        return false
    }

}

private enum Constants {

    static let horizontalMargin: CGFloat = 22

    static let insets = UIEdgeInsets(
        top: 30,
        left: Constants.horizontalMargin,
        bottom: 0,
        right: Constants.horizontalMargin
    )

    static let infoRows: [String] = [
        "Unlimites Posts & Stories!",
        "Split photos as you like!",
        "Create unlimited Spaces!",
        "Remove all Ads!"
    ]

    static let termsLink = "https://helpview.com/terms"

    static let privacyLink = "https://helpview.com/privacy"

    static let informationText = "Information about products not manufactured by Apple, or independent websites not controlled or tested by Apple, is provided without recommendation or endorsement. Apple assumes no responsibility with regard to the selection, performance, or use of third-party websites or products. Apple makes no representations regarding third-party website accuracy or reliability. Risks are inherent in the use of the Internet. Contact the vendor for additional information. Other company and product names may be trademarks of their respective owners."

}

