import UIKit

class SubscriptionOptionView: UIView {

    var onTap: VoidHandler?

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup

    private func adjustSubviews() {
        addSubviews([
            selectionImageView,
            durationLabel,
            priceLabel,
            trialLabel,
            touchView
        ])

        [durationLabel, priceLabel, trialLabel].forEach {
            $0.textAlignment = .center
        }

        trialLabel.numberOfLines = 0
        selectionImageView.backgroundColor = Palette.white
        
        touchView.addTarget(self, action: #selector(handleTap), for: .touchDown)

        layer.cornerRadius = 15
    }

    // MARK: - Views

    private lazy var touchView = UIControl()

    private lazy var selectionImageView = UIImageView()

    private lazy var durationLabel = UILabel(
        text: nil,
        textColor: Palette.black,
        font: Font.medium.withSize(16)
    )

    private lazy var priceLabel = UILabel(
        text: nil,
        textColor: Palette.black,
        font: Font.bold.withSize(21)
    )

    private lazy var trialLabel = UILabel(
        text: "FREE TRIAL",
        textColor: Palette.white,
        font: Font.bold.withSize(10)
    )

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        let insets = Constants.insets

        touchView.pin.all()

        selectionImageView.pin
            .size(CGSize(side: 30))
            .left(insets.left)
            .vCenter()

        priceLabel.pin
            .after(of: selectionImageView)
            .sizeToFit()
            .vCenter()
            .marginLeft(Screen.isIPhone5 ? 15 : 25)

        durationLabel.pin
            .after(of: priceLabel)
            .sizeToFit()
            .vCenter()

        trialLabel.pin
            .height(30)
            .width(72)
            .vCenter()
            .right(insets.right)

        trialLabel.roundize(cornerRadius: 15, clips: true)
        selectionImageView.roundize()
    }

    // MARK: - Actions

    @objc
    private func handleTap() {
        onTap?()
    }

    // MARK: - Model

    var model: PremiumSubscriptionsView.Option? {
        didSet {
            render()
        }
    }

    func render() {
        guard let model = model else { return }

        if model.isSelected == true {
            backgroundColor = Palette.white
            selectionImageView.image = UIImage(named: "premium-tick")
            trialLabel.backgroundColor = Palette.primary
            trialLabel.textColor = Palette.white
        } else {
            selectionImageView.image = nil
            backgroundColor = UIColor.hex("#F49490")
            trialLabel.backgroundColor = Palette.white
            trialLabel.textColor = Palette.primary
        }

        priceLabel.text = "\(model.price)$"
        durationLabel.text = "/\(model.duration.rawValue.lowercased())"
    }

}

private enum Constants {

    static let insets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 12)

    static let durationToPrice: CGFloat = 6

    static let priceToTrial: CGFloat = 5

}
