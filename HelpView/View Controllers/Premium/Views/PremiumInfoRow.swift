import UIKit

class PremiumInfoRow: UIView {

    let text: String

    init(text: String) {
        self.text = text

        super.init(frame: .zero)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        addSubview(dotView)
        addSubview(textLabel)
    }

    // MARK: - Views

    lazy var dotView = UIView(backgroundColor: Palette.white)

    lazy var textLabel = UILabel(text: text, textColor: Palette.white, font: Constants.textFont)

    // MARK: - Layout

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let labelWidth = size.width - Constants.dotSize.width - Constants.dotToText

        let height = max(
            Constants.dotSize.height,
            textLabel.boundingSize(for: labelWidth).height
        )

        return CGSize(width: textLabel.boundingSize(for: labelWidth).width + Constants.dotToText + Constants.dotSize.width, height: height)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        dotView.pin
            .size(Constants.dotSize)
            .vCenter()

        textLabel.pin
            .after(of: dotView)
            .marginLeft(Constants.dotToText)
            .right()
            .sizeToFit()
            .vCenter()

        dotView.roundize()
    }

}


private enum Constants {

    static let textFont = Font.regular.withSize(14)

    static let dotSize = CGSize(side: 3)

    static let dotToText: CGFloat = 10

}
