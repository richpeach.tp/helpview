import UIKit

final class TimePickerTransitionAnimator: NSObject {

    enum TransitionMode {
        case present
        case dismiss
    }

    let mode: TransitionMode

    init(mode: TransitionMode) {
        self.mode = mode

        super.init()
    }

}

extension TimePickerTransitionAnimator: UIViewControllerAnimatedTransitioning {

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let duration = transitionDuration(using: transitionContext)

        switch mode {
        case .present:
            guard let toViewController = transitionContext.viewController(forKey: .to) as? TimePickerViewController,
                  let toView = toViewController.view else { return }

            toView.frame = transitionContext.finalFrame(for: toViewController)
            containerView.addSubview(toView)

            toView.layoutIfNeeded()

            UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.5, options: .curveEaseOut) {
                toViewController.isContainerHidden = false
                toView.layoutIfNeeded()
            } completion: { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }

        case .dismiss:
            guard let fromViewController = transitionContext.viewController(forKey: .from) as? TimePickerViewController,
                  let fromView = fromViewController.view else { return }

            fromViewController.isContainerHidden = true

            UIView.animate(
                withDuration: duration,
                delay: 0,
                options: .curveEaseIn
            ) {
                fromView.layoutIfNeeded()
            } completion: { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
        }
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        mode == .present ? 0.4 : 0.15
    }

}
