import UIKit

final class TimePickerViewController: UIViewController {

    enum Mode {
        case date
        case time
    }

    var onSaveTap: ((Date) -> Void)?

    let initialDate: Date

    let mode: Mode

    init(initialDate: Date, mode: Mode) {
        self.initialDate = initialDate
        self.mode = mode
        super.init(nibName: nil, bundle: nil)

        self.transitioningDelegate = self
        self.modalPresentationStyle = .overFullScreen
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Views

    private let backgroundControl = UIControl()

    private let containerView = UIView()

    private lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()

        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }

        picker.date = initialDate
        picker.datePickerMode = mode == .date ? .date : .time
        return picker
    }()

    private var cancelButton: UIButton = {
        let cancelButton = UIButton(type: .system)
        cancelButton.titleLabel?.font = Font.medium.withSize(14)
        cancelButton.tintColor = Palette.white
        cancelButton.setTitle("CANCEL")
        cancelButton.addTarget(self, action: #selector(handleCloseTap), for: .touchUpInside)
        return cancelButton
    }()

    private var saveButton: UIButton = {
        let saveButton = UIButton(type: .system)
        saveButton.titleLabel?.font = Font.medium.withSize(14)
        saveButton.tintColor = Palette.white
        saveButton.setTitle("SAVE")
        saveButton.addTarget(self, action: #selector(handleSaveTap), for: .touchUpInside)
        return saveButton
    }()

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubviews([backgroundControl, containerView])
        containerView.addSubviews([cancelButton, datePicker, saveButton])

        containerView.backgroundColor = Palette.primary

        backgroundControl.addTarget(self, action: #selector(handleCloseTap), for: .touchUpInside)
    }

    // MARK: - Layout

    var isContainerHidden: Bool = true {
        didSet {
            view.setNeedsLayout()
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        backgroundControl.pin.all()

        let insets = Constants.insets
        let buttonHMargin = Constants.buttonHorizontalMargin

        containerView.pin
            .horizontally()
            .height(Constants.containerHeight)
            .bottom(isContainerHidden ? -Constants.containerHeight : 0)

        cancelButton.pin
            .top(insets.top)
            .left(buttonHMargin)
            .sizeToFit()

        saveButton.pin
            .top(insets.top)
            .right(buttonHMargin)
            .sizeToFit()

        datePicker.pin
            .horizontally()
            .below(of: cancelButton)
            .marginTop(Constants.buttonToPicker)
            .bottom()
    }

    // MARK: - Actions

    @objc private func handleCloseTap() {
        dismiss(animated: true, completion: nil)
    }

    @objc private func handleSaveTap() {
        onSaveTap?(datePicker.date)
        dismiss(animated: true, completion: nil)
    }

}


extension TimePickerViewController: UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        TimePickerTransitionAnimator(mode: .present)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        TimePickerTransitionAnimator(mode: .dismiss)
    }

}

private enum Constants {

    static let containerHeight: CGFloat = 245

    static let insets = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)

    static let buttonHorizontalMargin: CGFloat = 20

    static let buttonToPicker: CGFloat = 12
//    static let pickerInsets = UIEdgeInsets(top: <#T##CGFloat#>, left: <#T##CGFloat#>, bottom: <#T##CGFloat#>, right: <#T##CGFloat#>)

}

