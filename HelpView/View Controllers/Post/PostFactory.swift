import UIKit

struct PostFactory {

    let post: Post

    init(post: Post) {
        self.post = post
        self.caption = post.caption
        self.publishDate = post.publishDate
    }

    var caption: String?

    var publishDate: Date?

    // MARK: - Date

    private static var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        return formatter
    }()

    private static var timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter
    }()

    // MARK: - Generating

    func generateItems() -> [PostViewController.Item] {
        [
            .image(post.image),
            .input(.init(title: "Caption".uppercased(), text: caption, placeholder: "Add some text", isMultiline: true)),
            .date(.init(title: "Date".uppercased(), text: publishDate.map { PostFactory.dateFormatter.string(from: $0) })),
            .time(.init(title: "Time".uppercased(), text: publishDate.map { PostFactory.timeFormatter.string(from: $0) })),
            .delete
        ]
    }

}
