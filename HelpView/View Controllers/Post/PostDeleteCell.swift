import UIKit

class PostDeleteCell: UICollectionViewCell {

    var onTap: VoidHandler?

    // MARK: - Views

    lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = Palette.primary
        button.setTitle("DELETE POST")
        button.titleLabel?.font = Font.bold.withSize(18)
        button.setTitleColor(Palette.white, for: .normal)
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(handleTap), for: .touchUpInside)
        return button
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        contentView.addSubviews([button])
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        button.pin.all()
    }

    // MARK: - Actions

    @objc
    private func handleTap() {
        onTap?()
    }

}
