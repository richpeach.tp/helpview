import UIKit

final class PostViewController: UIViewController {

    var onSave: VoidHandler?

    var onDeleteTap: VoidHandler?

    let post: Post

    let imageStorage: ImageStorageProtocol

    let notificationService: NotificationServiceProtocol

    private lazy var factory = PostFactory(post: post)

    init(post: Post, imageStorage: ImageStorageProtocol, notificationService: NotificationServiceProtocol) {
        self.post = post
        self.imageStorage = imageStorage
        self.notificationService = notificationService
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView.emptyCollection()

        let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.scrollDirection = .vertical
        layout?.minimumInteritemSpacing = 0
        layout?.minimumLineSpacing = 16

        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = .clear

        return collectionView
    }()

    private lazy var collectionViewAdapter = PostViewAdapter(collectionView: collectionView, imageStorage: imageStorage)

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Palette.primaryBackground
        view.addSubview(collectionView)

        collectionViewAdapter.onCaptionChange = { [unowned self] caption in
            self.factory.caption = caption
            self.updateItems()
        }

        collectionViewAdapter.onTimeTap = { [unowned self] in
            showTimePicker(mode: .time)
        }

        collectionViewAdapter.onDateTap = { [unowned self] in
            showTimePicker(mode: .date)
        }

        collectionViewAdapter.onDeleteTap = { [unowned self] in
            AlertActionInteraction(title: "Are you sure you want to delete this post?", message: nil, action: UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
                self.onDeleteTap?()
            })).follow()
        }

        keyboardManager.delegate = self

        navigationItem.rightBarButtonItem = .saveItem(target: self, action: #selector(handleSave))

        render()
    }

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.pin.all()
    }

    // MARK: - Actions

    @objc
    private func handleTap() {
        view.endEditing(true)
    }

    @objc
    private func handleSave() {
        guard let publishDate = factory.publishDate else {
            AlertInteraction(title: "Unable to save", message: "Please select Date and Time to continue").follow()
            return
        }

        post.caption = factory.caption
        post.publishDate = publishDate

        notificationService.schedulePost(withId: post.id, at: publishDate) { [weak self] in
            self?.onSave?()
        }
    }

    // MARK: - Calendar

    private func showTimePicker(mode: TimePickerViewController.Mode) {
        let timePicker = TimePickerViewController(initialDate: factory.publishDate ?? Date(), mode: mode)

        timePicker.onSaveTap = { [unowned self] date in
            self.factory.publishDate = date
            self.render()
        }

        view.endEditing(true)
        present(timePicker, animated: true, completion: nil)
    }

    // MARK: - Render

    func updateItems() {
        collectionViewAdapter.setItems(factory.generateItems(), render: false)
    }

    func render() {
        updateItems()
        collectionViewAdapter.render()
    }

    // MARK: - Keyboard

    private lazy var keyboardManager = KeyboardManager()

}

extension PostViewController: KeyboardManagerDelegate {

    func keyboardManager(_ keyboardManager: KeyboardManager, keyboardWillChangeFrame frame: CGRect) {
        let isClosed = (Screen.height - frame.minY) <= 0

        var inset = collectionView.contentInset
        inset.bottom = isClosed ? 0 : frame.height
        collectionView.contentInset = inset
    }

}

extension PostViewController {

//    struct ViewModel {
//        let items: [Item]
//    }

    enum Item {
        case image(Post.Image)
        case input(InputCell.ViewModel)
        case date(TimeCell.ViewModel)
        case time(TimeCell.ViewModel)
        case delete
    }

}

private enum Constants {

}

