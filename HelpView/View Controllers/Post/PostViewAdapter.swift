import UIKit

class PostViewAdapter: NSObject {

    var onDeleteTap: VoidHandler?

    var onDateTap: VoidHandler?

    var onTimeTap: VoidHandler?

    var onCaptionChange: ((String) -> Void)?

    let collectionView: UICollectionView

    let imageStorage: ImageStorageProtocol

    init(collectionView: UICollectionView, imageStorage: ImageStorageProtocol) {
        self.collectionView = collectionView
        self.imageStorage = imageStorage

        super.init()

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        collectionView.register(PostImageCell.self, forCellWithReuseIdentifier: PostImageCell.reuseIdentifier)
        collectionView.delaysContentTouches = false
        collectionView.register(TimeCell.self, forCellWithReuseIdentifier: TimeCell.reuseIdentifier)
        collectionView.register(InputCell.self, forCellWithReuseIdentifier: InputCell.reuseIdentifier)
        collectionView.register(PostDeleteCell.self, forCellWithReuseIdentifier: PostDeleteCell.reuseIdentifier)
    }

    // MARK: - Items

    private(set) var items: [PostViewController.Item] = []

//    var items: [PostViewController.Item] = [] {
//        didSet {
//            collectionView.reloadData()
//        }
//    }

    func setItems(_ items: [PostViewController.Item], render: Bool) {
        self.items = items

        if render {
            self.render()
        }
    }

    func render() {
        collectionView.reloadData()
    }

}

extension PostViewAdapter: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.item]

        switch item {
        case .image(let image):
            let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: PostImageCell.reuseIdentifier, for: indexPath) as! PostImageCell

            imageStorage.retrieveImage(forKey: image.key) { image in
                imageCell.imageView.image = image
            }

            return imageCell

        case .input(let viewModel):
            let inputCell = collectionView.dequeueReusableCell(withReuseIdentifier: InputCell.reuseIdentifier, for: indexPath) as! InputCell

            inputCell.viewModel = viewModel

            inputCell.onInputChange = { [unowned self] _ in
                self.onCaptionChange?(inputCell.inputText)
                collectionView.collectionViewLayout.invalidateLayout()
                collectionView.layoutIfNeeded()
                collectionView.scrollRectToVisible(inputCell.frame, animated: true)
            }

            return inputCell

        case .date(let viewModel):
            let dateCell = collectionView.dequeueReusableCell(withReuseIdentifier: TimeCell.reuseIdentifier, for: indexPath) as! TimeCell
            dateCell.viewModel = viewModel
            return dateCell

        case .time(let viewModel):
            let timeCell = collectionView.dequeueReusableCell(withReuseIdentifier: TimeCell.reuseIdentifier, for: indexPath) as! TimeCell
            timeCell.viewModel = viewModel
            return timeCell

        case .delete:
            let deleteCell = collectionView.dequeueReusableCell(withReuseIdentifier: PostDeleteCell.reuseIdentifier, for: indexPath) as! PostDeleteCell

            deleteCell.onTap = { [unowned self] in
                onDeleteTap?()
            }

            return deleteCell
        }
    }

}

extension PostViewAdapter: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch items[indexPath.item] {
        case .date:
            onDateTap?()

        case .time:
            onTimeTap?()

        default:
            break
        }
    }

}

extension PostViewAdapter: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width - 22 - 22
        let item = items[indexPath.item]

        switch item {
        case .image(let image):
            return image.size.aspectFitSize(CGSize(width: cellWidth, height: .greatestFiniteMagnitude))

        case .input(let viewModel):
            let height = InputCell.height(for: viewModel, maximumWidth: cellWidth)
            return CGSize(width: cellWidth, height: height)

        case .date(let viewModel):
            let height = TimeCell.height(for: viewModel, maximumWidth: cellWidth)
            return CGSize(width: cellWidth, height: height)

        case .time(let viewModel):
            let height = TimeCell.height(for: viewModel, maximumWidth: cellWidth)
            return CGSize(width: cellWidth, height: height)

        case .delete:
            return CGSize(width: cellWidth, height: 50)
        }

    }

}
