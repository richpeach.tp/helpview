import UIKit

final class CalendarViewController: UIViewController {

    var onBackTap: VoidHandler?

    var onPostsTap: ((Date, [Post]) -> Void)?

    let feedService: FeedServiceProtocol

    let posts: [Post]

    init(currentSpace: Space, feedService: FeedServiceProtocol) {
        self.feedService = feedService
        self.posts = feedService.feed(with: currentSpace.id, andKind: .posts).posts
        self.datesWithPosts = self.posts.compactMap { $0.publishDate }

        super.init(nibName: nil, bundle: nil)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Views

    private let collectionView = UICollectionView.emptyCollection()

    private lazy var collectionViewAdapter = CalendarsViewAdapter(collectionView: collectionView)

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Palette.primaryBackground
        view.addSubview(collectionView)

        collectionViewAdapter.viewModels = generateViewModels()

        collectionViewAdapter.onDayTap = { [unowned self] date in
            let postsOnThisDay = posts.filter {
                $0.publishDate != nil && Calendar.current.isDate($0.publishDate!, inSameDayAs: date)
            }

            if !postsOnThisDay.isEmpty {
                onPostsTap?(postsOnThisDay.first!.publishDate!, postsOnThisDay)
            }
        }

        setupNavigationBar(backButtonAction: #selector(handleBackTap))
    }

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.pin.all()
    }

    // MARK: - Actions

    @objc
    private func handleBackTap() {
        onBackTap?()
    }

    // MARK: - Dates

    private var datesWithPosts: [Date]

    private func generateBaseDates() -> [Date] {
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()

        var dates = [currentDate]

        for offset in 1...12 {
            let date = calendar.date(byAdding: .month, value: offset, to: currentDate)
            date.map { dates.append($0) }
        }

        return dates
    }

    private func generateViewModels() -> [CalendarView.ViewModel] {
        let baseDates = generateBaseDates()

        return baseDates.map {
            CalendarView.ViewModel(baseDate: $0, selectableDates: datesWithPosts)
        }
    }

}

private enum Constants {

}
