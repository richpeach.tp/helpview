import UIKit

extension CalendarView {

    struct ViewModel {
        let baseDate: Date
        let selectableDates: [Date]
    }

}

class CalendarView: UIView {

    var onDayTap: ((Date) -> Void)? {
        didSet {
            collectionViewAdapter.onDayTap = onDayTap
        }
    }

    // MARK: - Calendar

    let viewModel: ViewModel

    var baseDate: Date {
        viewModel.baseDate
    }

    var seletableDates: [Date] {
        viewModel.selectableDates
    }

    private let calendar = Calendar(identifier: .gregorian)

    private var days: [Day] = [] {
        didSet {
            collectionViewAdapter.daysViewModels = days.map(DayCell.ViewModel.init)
        }
    }

    private static func numberOfWeeks(inBaseDate baseDate: Date, calendar: Calendar = Calendar(identifier: .gregorian)) -> Int {
        calendar.range(of: .weekOfMonth, in: .month, for: baseDate)?.count ?? 0
    }

    // MARK: - Views

    private let collectionView = UICollectionView.emptyCollection()

    private lazy var collectionViewAdapter = CalendarViewAdapter(collectionView: collectionView)

    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d"
        return formatter
    }()

    private let headerView = HeaderView()

    // MARK: - Init

    init(viewModel: ViewModel) {
        self.viewModel = viewModel

        super.init(frame: .zero)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        backgroundColor = .white
        addSubview(collectionView)
        addSubview(headerView)

        layer.cornerRadius = 20

        days = generateDaysInMonth(for: baseDate)
        headerView.baseDate = baseDate
    }

    // MARK: - Layout

    static func height(for baseDate: Date) -> CGFloat {
        let weeksCount = CGFloat(Self.numberOfWeeks(inBaseDate: baseDate))
        let daysHeight = weeksCount * Constants.dayHeight + (weeksCount - 1) * Constants.interWeekSpacing
        let headerHeight = HeaderView.height
        return headerHeight + daysHeight + Constants.insets.vertical
    }

    var height: CGFloat {
        Self.height(for: baseDate)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let insets = Constants.insets

        headerView.pin
            .top(insets.top)
            .left(insets.left)
            .right(insets.right)
            .height(HeaderView.height)

        collectionView.pin
            .below(of: headerView)
            .left(insets.left)
            .right(insets.right)
            .bottom(insets.bottom)
    }

    // MARK: - Actions

}

private extension CalendarView {

    func monthMetadata(for baseDate: Date) throws -> MonthMetadata {
        guard
            let numberOfDaysInMonth = calendar.range(of: .day, in: .month, for: baseDate)?.count,
            let firstDayOfMonth = calendar.date(from: calendar.dateComponents([.year, .month], from: baseDate))
        else {
            throw CalendarDataError.metadataGeneration
        }

        let firstDayWeekday = calendar.component(.weekday, from: firstDayOfMonth)

        return MonthMetadata(
            numberOfDays: numberOfDaysInMonth,
            firstDay: firstDayOfMonth,
            firstDayWeekday: firstDayWeekday
        )
    }

    func generateDaysInMonth(for baseDate: Date) -> [Day] {
        guard let metadata = try? monthMetadata(for: baseDate) else {
            fatalError("An error occurred when generating the metadata for \(baseDate)")
        }

        let numberOfDaysInMonth = metadata.numberOfDays
        let offsetInInitialRow = metadata.firstDayWeekday
        let firstDayOfMonth = metadata.firstDay

        var days: [Day] = (1..<(numberOfDaysInMonth + offsetInInitialRow)).map { day in
            let isWithinDisplayedMonth = day >= offsetInInitialRow
            let dayOffset = isWithinDisplayedMonth ? day - offsetInInitialRow : -(offsetInInitialRow - day)

            return generateDay(
                offsetBy: dayOffset,
                for: firstDayOfMonth,
                isWithinDisplayedMonth: isWithinDisplayedMonth
            )
        }

        days += generateStartOfNextMonth(using: firstDayOfMonth)

        return days
    }

    func generateDay(
        offsetBy dayOffset: Int,
        for baseDate: Date,
        isWithinDisplayedMonth: Bool
    ) -> Day {
        let date = calendar.date(byAdding: .day, value: dayOffset, to: baseDate) ?? baseDate
        let isSelectable = seletableDates.map { calendar.isDate(date, inSameDayAs: $0) }.contains(true)

        return Day(
            date: date,
            number: dateFormatter.string(from: date),
            isSelectable: isSelectable,
            isWithinDisplayedMonth: isWithinDisplayedMonth
        )
    }

    func generateStartOfNextMonth(
        using firstDayOfDisplayedMonth: Date
    ) -> [Day] {
        guard let lastDayInMonth = calendar.date(byAdding: DateComponents(month: 1, day: -1), to: firstDayOfDisplayedMonth) else {
            return []
        }

        let additionalDays = 7 - calendar.component(.weekday, from: lastDayInMonth)

        guard additionalDays > 0 else { return [] }

        let days: [Day] = (1...additionalDays).map {
            generateDay(
                offsetBy: $0,
                for: lastDayInMonth,
                isWithinDisplayedMonth: false
            )
        }

        return days
    }

    enum CalendarDataError: Error {
        case metadataGeneration
    }

}

extension CalendarView {

    enum Constants {
        static let insets = UIEdgeInsets(top: 20, left: 12, bottom: 20, right: 12)
        static let interDaySpacing: CGFloat = 6
        static let interWeekSpacing: CGFloat = 6
        static let dayHeight: CGFloat = 40
        static let dayOfWeekHeight: CGFloat = 40
        static let monthLabelHeight: CGFloat = 20
        static let monthLabelToDayOfWeek: CGFloat = 12
    }

}
