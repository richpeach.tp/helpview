import UIKit

final class CalendarViewAdapter: NSObject {

    var onDayTap: ((Date) -> Void)?

    var daysViewModels: [CalendarView.DayCell.ViewModel] = []

    let collectionView: UICollectionView

    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        
        super.init()

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumInteritemSpacing = CalendarView.Constants.interDaySpacing
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing = CalendarView.Constants.interWeekSpacing
        collectionView.register(CalendarView.DayCell.self, forCellWithReuseIdentifier: CalendarView.DayCell.reuseIdentifier)
    }
}

extension CalendarViewAdapter: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        daysViewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CalendarView.DayCell.reuseIdentifier, for: indexPath) as! CalendarView.DayCell
        cell.viewModel = daysViewModels[indexPath.item]
        return cell
    }

}

extension CalendarViewAdapter: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        onDayTap?(daysViewModels[indexPath.item].date)
    }

}

extension CalendarViewAdapter: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Int((collectionView.frame.width - CalendarView.Constants.interDaySpacing * 6) / 7)
        let height = Int(CalendarView.Constants.dayHeight)
        return CGSize(width: width, height: height)
    }

}

