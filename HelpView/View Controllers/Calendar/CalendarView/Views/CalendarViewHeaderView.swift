import UIKit

extension CalendarView {

    class HeaderView: UIView {

        var baseDate: Date? {
            didSet {
                _ = baseDate.map {
                    monthLabel.text = dateFormatter.string(from: $0).uppercased()
                }

                setNeedsLayout()
            }
        }

        // MARK: - Views

        private lazy var dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMMM yyyy"
            return formatter
        }()

        lazy var monthLabel: UILabel = {
            UILabel(text: nil, textColor: Palette.primary, font: Font.bold.withSize(17))
        }()

        lazy var dayOfWeekStackView: UIStackView = {
            let stackView = UIStackView()
            stackView.distribution = .fillEqually
            stackView.spacing = Constants.interDaySpacing
            return stackView
        }()

        // MARK: - Init

        override init(frame: CGRect) {
            super.init(frame: frame)

            adjustSubviews()
        }

        required init?(coder: NSCoder) {
            super.init(coder: coder)
        }

        // MARK: - Adjust

        private func adjustSubviews() {
            addSubviews([monthLabel, dayOfWeekStackView])
            adjustDays()
        }

        private func adjustDays() {
            for dayNumber in 1...7 {
                let dayLabel = UILabel(text: dayOfWeekLetter(for: dayNumber), textColor: Palette.primary, font: Font.bold.withSize(17))
                dayLabel.textAlignment = .center
                dayOfWeekStackView.addArrangedSubview(dayLabel)
            }
        }

        // MARK: - Layout

        static var height: CGFloat {
            Constants.monthLabelHeight + Constants.monthLabelToDayOfWeek + Constants.dayOfWeekHeight 
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            monthLabel.pin.sizeToFit().right()

            dayOfWeekStackView.pin
                .bottom()
                .horizontally()
                .height(Constants.dayOfWeekHeight)
        }

    }

}

extension CalendarView.HeaderView {

    private func dayOfWeekLetter(for dayNumber: Int) -> String {
        switch dayNumber {
        case 1:
            return "S"
        case 2:
            return "M"
        case 3:
            return "T"
        case 4:
            return "W"
        case 5:
            return "T"
        case 6:
            return "F"
        case 7:
            return "S"
        default:
            return ""
        }
    }

}

private enum Constants {

}
