import UIKit

extension CalendarView {

    class DayCell: UICollectionViewCell {

        struct ViewModel {
            let date: Date
            let number: String
            let isWithinMonth: Bool
            let isSelectable: Bool

            init(from day: Day) {
                self.date = day.date
                self.isWithinMonth = day.isWithinDisplayedMonth
                self.number = day.number
                self.isSelectable = day.isSelectable
            }
        }

        // MARK: - Views

        private let numberLabel = UILabel(text: nil, textColor: Palette.black, font: Font.regular.withSize(14))

        // MARK: - Init

        override init(frame: CGRect) {
            super.init(frame: frame)

            adjustSubviews()
        }

        required init?(coder: NSCoder) {
            super.init(coder: coder)
        }

        // MARK: - Adjust

        private func adjustSubviews() {
            addSubview(numberLabel)

            numberLabel.textAlignment = .center
            numberLabel.layer.cornerRadius = 10
            numberLabel.clipsToBounds = true
        }

        // MARK: - Layout

        override func layoutSubviews() {
            super.layoutSubviews()

            numberLabel.pin.all()
        }

        // MARK: - View Model

        var viewModel: ViewModel? {
            didSet {
                numberLabel.text = viewModel?.number
                numberLabel.alpha = viewModel?.isWithinMonth == true ? 1 : 0.5

                if viewModel?.isSelectable == true {
                    numberLabel.backgroundColor = Palette.primary
                    numberLabel.textColor = Palette.white
                } else {
                    numberLabel.backgroundColor = .clear
                    numberLabel.textColor = Palette.black
                }
            }
        }

    }

}

private enum Constants {

}
