import Foundation

extension CalendarView {

    struct MonthMetadata {
      let numberOfDays: Int
      let firstDay: Date
      let firstDayWeekday: Int
    }

}
