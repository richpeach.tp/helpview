import Foundation

extension CalendarView {

    struct Day {
        let date: Date
        let number: String
        let isSelectable: Bool
        let isWithinDisplayedMonth: Bool
    }

}
