import UIKit

final class CalendarsViewAdapter: NSObject {

    var onDayTap: ((Date) -> Void)?

    var viewModels: [CalendarView.ViewModel] = []

    let collectionView: UICollectionView

    init(collectionView: UICollectionView) {
        self.collectionView = collectionView

        super.init()

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing = 16
        collectionView.register(CalendarViewCell.self, forCellWithReuseIdentifier: CalendarViewCell.reuseIdentifier)
    }
}

extension CalendarsViewAdapter: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CalendarViewCell.reuseIdentifier, for: indexPath) as! CalendarViewCell
        cell.viewModel = viewModels[indexPath.item]
        cell.onDayTap = onDayTap
        return cell
    }

}

extension CalendarsViewAdapter: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = CalendarViewCell.height(for: viewModels[indexPath.item].baseDate)
        let width = collectionView.bounds.width - 16 - 16
        return CGSize(width: width, height: height)
    }

}

