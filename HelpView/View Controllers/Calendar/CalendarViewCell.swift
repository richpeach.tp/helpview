import UIKit

final class CalendarViewCell: UICollectionViewCell {

    var onDayTap: ((Date) -> Void)? {
        didSet {
            calendarView?.onDayTap = onDayTap
        }
    }

    private(set) var calendarView: CalendarView? {
        didSet {
            oldValue?.removeFromSuperview()
            _ = calendarView.map { contentView.addSubview($0) }
        }
    }

    var viewModel: CalendarView.ViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }

            calendarView = CalendarView(viewModel: viewModel)
            contentView.addSubview(self.calendarView!)
        }
    }

    static func height(for baseDate: Date) -> CGFloat {
        CalendarView.height(for: baseDate)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        calendarView?.frame = bounds
    }

}
