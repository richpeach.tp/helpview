import UIKit

extension PostListCell {

    struct ViewModel {

        let image: Post.Image
        let caption: String?
        let time: String

        init(post: Post) {
            self.image = post.image
            self.caption = post.caption
            self.time = PostListCell.dateFormatter.string(from: post.publishDate!)
        }
        
    }

}

class PostListCell: UICollectionViewCell {

    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter
    }()

    // MARK: - Views

    lazy var imageView = UIImageView()

    lazy var captionTitleLabel = UILabel(text: "CAPTION", textColor: Palette.black, font: Font.bold.withSize(14))

    lazy var captionLabel = UILabel(text: nil, textColor: Palette.black, font: Font.regular.withSize(14))

    lazy var timeTitleLabel = UILabel(text: "TIME", textColor: Palette.black, font: Font.bold.withSize(14))

    lazy var timeLabel = UILabel(text: nil, textColor: Palette.primary, font: Font.bold.withSize(14))

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Adjust

    private func adjustSubviews() {
        backgroundColor = Palette.white
        layer.cornerRadius = 15

        imageView.roundize(cornerRadius: 14, clips: true)
        imageView.contentMode = .scaleAspectFill

        captionLabel.numberOfLines = 2

        flex.direction(.row).padding(Constants.insets).define { flex in
            flex.addItem(imageView).aspectRatio(1)

            flex.addItem().direction(.column).justifyContent(.spaceBetween).marginLeft(12).grow(1).define { flex in
                flex.addItem().define { flex in
                    flex.addItem(captionTitleLabel)
                    flex.addItem(captionLabel).marginTop(4)
                }

                flex.addItem().define { flex in
                    flex.addItem().direction(.row).define { flex in
                        flex.addItem(timeTitleLabel)
                        flex.addItem(timeLabel).marginLeft(8)
                    }
                }
            }
        }
    }

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        flex.layout()
    }

    // MARK: - View Model

    var viewModel: ViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }

            captionLabel.text = viewModel.caption
            timeLabel.text = viewModel.time

            setNeedsLayout()
        }
    }

}

private enum Constants {
    static let insets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)

}

