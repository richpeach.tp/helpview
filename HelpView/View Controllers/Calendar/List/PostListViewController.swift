import UIKit

final class PostListViewController: UIViewController {

    var onBackTap: VoidHandler?
    
    var onTap: ((Post) -> Void)?

    let date: Date

    let posts: [Post]

    let imageStorage: ImageStorageProtocol

    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d yyyy"
        return formatter
    }()

    init(date: Date, posts: [Post], imageStorage: ImageStorageProtocol) {
        self.date = date
        self.posts = posts
        self.imageStorage = imageStorage
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var items: [PostListCell.ViewModel] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        title = dateFormatter.string(from: date)

        view.backgroundColor = Palette.primaryBackground
        view.addSubview(collectionView)

        items = posts.map(PostListCell.ViewModel.init)

        setupNavigationBar(backButtonAction: #selector(handleBackTap))
    }

    // MARK: - Views

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView.emptyCollection()

        let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.scrollDirection = .vertical
        layout?.minimumInteritemSpacing = 0
        layout?.minimumLineSpacing = 16

        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)

        collectionView.register(PostListCell.self, forCellWithReuseIdentifier: PostListCell.reuseIdentifier)

        return collectionView
    }()

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.pin
            .all()
    }

    // MARK: - Actions

    @objc
    private func handleBackTap() {
        onBackTap?()
    }


}

extension PostListViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PostListCell.reuseIdentifier, for: indexPath) as! PostListCell
        cell.viewModel = item

        imageStorage.retrieveImage(forKey: item.image.key) { image in
            cell.imageView.image = image
        }
        
        return cell
    }

}

extension PostListViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let post = posts[indexPath.item]
        onTap?(post)
    }

}

extension PostListViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.bounds.width - 16 - 16, height: 124)
    }

}
