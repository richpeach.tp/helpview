import UIKit

class TermsViewController: ParagraphsViewController {

    init() {
        super.init(paragraphs: Constants.termsParagraphs)

        self.title = "Terms of Use"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

//        navigationController?.navigationBar.makeWhiteContent()
//        navigationController?.navigationBar.makeBigWhiteTitle()
    }

}

private enum Constants {

    static let termsNew = [
        "The following document represents the only legitimate and relevant version of Terms of Use of HelpView. If the user doesn’t accept these statements, then he is forbidden to use the service. He is also welcome to get in touch with the owner of the application or the Technical Support in order to solve this issue.\n",

        "\\tIntroduction\n",

        "This document establishes and governs proper relationships between the official owner and the actual user of HelpView. Although the mentioned terms relate only to actual users, the potential ones should also read them in advance. Basically, Terms of Use text represents a lawful agreement, which should be accepted by the user before the installation. If someone intends to use the application we created and distribute, then he is recommended to read the following paragraphs thoroughly. We warn the user ahead that in case he installs and launches HelpView, this means that he fully accepts each term.\n",

        "The Terms of Use document was created and shared for several reasons. Basically, it is required for:",
        "\\binforming the user about the most relevant terms of service, forbidden actions, denial policy, etc,",
        "\\bestablishing and controlling lawful relationships between all the participants (the user and the owner of HelpView),",
        "\\bpreventing possible misunderstanding and law disputes by clarifying all the nuances of service.\n",

        "\\tMain terms\n",

        "Denial policy. The owner of HelpView reserves the right to deny service at his sole discretion at any moment. This may be done in case the user doesn’t follow the mentioned terms, violates the owner’s rights, uses our product illegally or commits a banned action.\n",

        "Updates. As it was already written, the current document’s version is the only relevant one. However, it may be modified, renewed or replaced with another text by the owner of HelpView. The owner isn’t limited anyhow - the update may take place at any moment, for unlimited times and without warning. The new version becomes legitimate the day of sharing.\n",

        "The rights of the app owner. The owner of HelpView reserves the right to complain to a competent authority if he experiences any kind of violation from the user. This will be done in order to protect the owner’s data, intellectual property, rights and so on.\n",

        "\\tForbidden actions\n",

        "Attention! All the actions mentioned in this paragraph will be punished by law immediately, as they violate the owner’s rights. They may harm/destroy his intellectual or physical property. In addition, all these actions are a reason for service denial.",
        "\\bThe user is strictly forbidden to commit (wittingly or accidentally) any actions that lead to the application’s destruction.",
        "\\bThe user has no right to modify the application in any way, change its code, parts and so on. This relates to both - the entire product and its separate parts.",
        "\\bIn addition, the user is also prohibited to interact with the source code of HelpView. He is forbidden to modify or extract it.",
        "\\bThe commitment of any harmful actions regarding the application or related materials is strictly forbidden.",
        "\\bThe user isn’t allowed to copy this unique program, website and other connected materials."
    ]

    static var termsParagraphs: [Paragraph] {
        termsNew.map { string in
            var text = string.components(separatedBy: "\\b").last!
            text = text.components(separatedBy: "\\t").last!
            return Paragraph(text: text, isBullet: string.starts(with: "\\b"), isTitle: string.starts(with: "\\t"))
        }
    }

}
