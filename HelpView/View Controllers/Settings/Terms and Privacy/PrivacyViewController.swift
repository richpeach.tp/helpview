import UIKit

class PrivacyViewController: ParagraphsViewController {

    init() {
        super.init(paragraphs: Constants.privacyParagraphs)

        self.title = "Privacy Policy"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

//        navigationController?.navigationBar.makeWhiteContent()
//        navigationController?.navigationBar.makeBigWhiteTitle()
    }

}

private enum Constants {

    static let privacyNew = [
        "The Privacy Policy document governs all the actions and procedures regarding personal data gathering, processing and sharing. The text was created and published in order to establish, control and regulate lawful relationships between the actual/potential users and the owner of HelpView. This Policy also informs those, who only intend to use the application, about all the optional/compulsory processes regarding personal information usage that may take place.\n",

        "\\tWhy is your data required?\n",
        "In short, the owner of HelpView has to gather the user’s personal (identifying) and non-personal (technical) information for different reasons. They all are united by one main aim - better service providing and its continuous improvement.\n",
        "We collect and store the data about sessions, suspicious actions to track possible fraudulent activity and assure the highest protection level for both sides - us and the user.\n",
        "Information about the user’s in-app or on-website activity is also required for analytical and statistical purposes. We need to know in which ways the user interacts with the service. The third-party analytical services we refer to also help us improve marketing campaigns, distribution, promotion and the app itself. So, we can make the usage of HelpView more convenient and user-oriented.\n",
        "One more important purpose is proper communication with every unique user. Thus, we need to identify him correctly.\n",

        "\\tData we need to gather:\n",
        "\\bPersonal or identifying data. The one we use to identify the unique user during the service providing. Originally, this list includes: the user’s name, sex, age, geolocation and other statements.",
        "\\bTechnical information or main device requirements. This data type is extremely important for customization of our service according to the device you use. Basically, we need to collect the iOS version, hardware model, screen resolution, etc.",
        "\\bCrash information. The Log data file is sent to us automatically the moment you face errors during the usage. For example, if something goes wrong while the app is launched, or it just shuts down, this piece of data will help us detect the problem and solve it sooner.",
        "\\bUser’s in-app or on-website activity. We track how exactly the user interacts with our digital products - the application and this website. Consequently, we collect such metrics as the number of sessions per day and their approximate duration, etc.",
        "\\bWe don’t gather any kind or sensitive information from the user.\n",

        "More about Cookies. We have to inform the user about this policy in advance. Cookies and similar tracking technologies are applied each time he interacts with our products - this particular website and the application itself. They are applied automatically. Nevertheless, this option may be switched off manually from the Settings. Cookies and similar technologies help us provide the most appropriate and convenient service according to the user’s needs, preferences, dislikes and recent actions.\n",

        "\\tRetention time and place\n",
        "The owner of HelpView has a right to keep all the gathered information (supplied by the user himself or obtained automatically) for as long as the service requires. It will be kept in our databases. All the received information may be processed in both - the owner’s head office and other possible places.\n",

        "\\tData protection\n",
        "We warn the user in advance that no ideal methods of data protection exist. Our team always does its best to keep the user’s personal and non-personal information away from third parties and fully safe while transmitting and processing. Furthermore, the owner of HelpView has a right to apply all the known and proven methods to ensure the highest level of data protection.\n",

        "\\tRequired permissions\n",
        "Gallery. HelpView requires this permission for correct work of the service, which fully depends on uploading and downloading images by the user.\n",
        "Notifications. We ask the user to allow sending him push-notifications. This helps us keep in touch with the user.\n",

        "\\tUser’s main rights\n",
        "\\bThe user should be fully aware of his rights to take measures in time.",
        "\\bThe user is free to cancel any permissions at his sole discretion if they were given before. However, we warn you ahead that the functionality of the application may be shortened because of these permissions.",
        "\\bThe user has a right to object data usage at any moment in case some illegal actions are detected.",
        "\\bThe user can always request for a copy of the already gathered information, which is stored in our database.",
        "\\bHe is free to ask the owner of HelpView to update the user’s personal data if the information loses its relevance.",
        "\\bThe data we collected or obtained may be fully deleted from our database after the user’s personal request.",
        "\\bThe user can always forbid further data usage without explanation. Consequently, the app owner will be able only to collect and store such information without processing and usage.",
        "\\bThe user has a right to complain to the competent authority asking for lawful protection.\n",

        "\\tPolicy regarding updates\n",
        "This is the only relevant Privacy Policy of HelpView. Nevertheless, the app owner reserves the right to perform all the actions that lead to the documents’ modification at his sole discretion. For example, if this is required for better service providing or because of the app’s update. None of such actions require direct notifying.\n",

        "If you wish to stay informed and be aware of possible updates in advance, then you are recommended to renew the current web page regularly. Please, notice that all the updated terms should be accepted before continuing the usage. If the user launches HelpView after the update of this Policy, this means that he fully agrees with all the paragraphs. The new version becomes legitimate the day of sharing.\n",

        "\\tContacts\n",
        "We are open and free to discuss all the moments regarding personal data usage if you have such questions. Please, use the available contacts from the matching page of this website to get in touch with the owner of HelpView or our Support."
    ]

    static var privacyParagraphs: [Paragraph] {
        privacyNew.map { string in
            var text = string.components(separatedBy: "\\b").last!
            text = text.components(separatedBy: "\\t").last!
            return Paragraph(text: text, isBullet: string.starts(with: "\\b"), isTitle: string.starts(with: "\\t"))
        }
    }

}
