import UIKit

struct Paragraph {
    let text: String
    let isBullet: Bool
    let isTitle: Bool

    init(text: String, isBullet: Bool = false, isTitle: Bool = false) {
        self.text = text
        self.isTitle = isTitle
        self.isBullet = isBullet
    }
}

class ParagraphsViewController: UIViewController {

    let paragraphs: [Paragraph]

    init(paragraphs: [Paragraph]) {
        self.paragraphs = paragraphs
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView.emptyCollection()

        let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.scrollDirection = .vertical
        layout?.minimumInteritemSpacing = 0
        layout?.minimumLineSpacing = 0

        collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        collectionView.backgroundColor = .clear
        collectionView.alwaysBounceVertical = true
        collectionView.clipsToBounds = true

        return collectionView
    }()

    private lazy var collectionViewAdapter = ParagraphsAdapter(collectionView: collectionView)

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Palette.primary
        view.addSubview(collectionView)

        collectionViewAdapter.paragraphs = paragraphs
    }

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.pin.all()
    }

}

private class ParagraphsAdapter: NSObject {

    let collectionView: UICollectionView

    var paragraphs: [Paragraph] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    init(collectionView: UICollectionView) {
        self.collectionView = collectionView

        super.init()

        collectionView.register(ParagraphCell.self, forCellWithReuseIdentifier: ParagraphCell.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
    }

}

extension ParagraphsAdapter: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        paragraphs.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let paragraph = paragraphs[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ParagraphCell.reuseIdentifier, for: indexPath) as! ParagraphCell
        cell.paragraph = paragraph
        return cell
    }

}

extension ParagraphsAdapter: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paragraph = paragraphs[indexPath.item]
        let height = ParagraphCell.height(for: paragraph, width: collectionView.bounds.width)
        return CGSize(width: collectionView.bounds.width, height: height)
    }

}
