import UIKit

class ParagraphCell: UICollectionViewCell {

    var paragraph: Paragraph? {
        didSet {
            var text = paragraph?.text

            if paragraph?.isTitle == true {
                text = text?.uppercased()
            }

            textLabel.text = text
            textLabel.font = paragraph?.isTitle == true ? Constants.titleFont : Constants.textFont
            bulletView.isHidden = paragraph?.isBullet == false
            setNeedsLayout()
        }
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        adjustSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup

    private func adjustSubviews() {
        addSubviews([bulletView, textLabel])

        textLabel.numberOfLines = 0
    }

    // MARK: - Views

    private lazy var textLabel = UILabel(
        text: nil,
        textColor: Palette.white,
        font: Font.regular.withSize(12)
    )

    private lazy var bulletView = UIView(backgroundColor: Palette.white)

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        let insets = Constants.insets

        if bulletView.isHidden {
            textLabel.pin.all(insets)
        } else {
            bulletView.pin
                .size(CGSize(side: 4))
                .top(6 + insets.top)
                .left(insets.left)

            textLabel.pin
                .after(of: bulletView)
                .marginLeft(8)
                .right(insets.right)
                .top(insets.top)
                .bottom(insets.bottom)
        }

        bulletView.roundize()
    }

    static func height(for paragraph: Paragraph, width: CGFloat) -> CGFloat {
        let insets = Constants.insets.horizontal
        let font = paragraph.isTitle ? Constants.titleFont : Constants.textFont

        if paragraph.isBullet {
            let textWidth = width - Constants.bulletTextOffset
            return paragraph.text.boundingSize(forWidth: textWidth - insets, attributes: [.font: font]).height
        }

        return paragraph.text.boundingSize(forWidth: width - insets, attributes: [.font: font]).height
    }

}

private enum Constants {

    static let insets = UIEdgeInsets(top: 0, left: 22, bottom: 0, right: 22)

    static let bulletTextOffset: CGFloat = 4 + 8

    static let textFont = Font.regular.withSize(12)

    static let titleFont = Font.bold.withSize(16)

}
