import UIKit

class SettingsItem {

    enum ID {
        case notification
        case terms
        case privacy
        case support
        case premium
    }

    enum `Type` {
        case switcher(Bool)
        case next
    }

    let id: ID
    let type: Type

    init(id: ID, type: Type) {
        self.id = id
        self.type = type
    }

}

extension SettingsItem.ID {

    var title: String {
        switch self {
        case .premium:
            return "Try Premium"
        case .notification:
            return "Notification"
        case .privacy:
            return "Privacy Policy"
        case .support:
            return "Support"
        case .terms:
            return "Terms of use"
        }
    }

    var icon: UIImage? {
        switch self {
        case .premium:
            return UIImage(named: "premium")
        case .notification:
            return UIImage(named: "notification")
        case .privacy:
            return UIImage(named: "privacy")
        case .support:
            return UIImage(named: "support")
        case .terms:
            return UIImage(named: "terms")
        }
    }

}

class SettingsViewController: UIViewController {

    var onTap: ((SettingsItem.ID) -> Void)? {
        didSet {
            collectionViewAdapter.onTap = onTap
        }
    }

    var onClose: VoidHandler?

    let userSettings: UserSettingsProtocol

    var notificationService: NotificationServiceProtocol

    init(userSettings: UserSettingsProtocol, notificationService: NotificationServiceProtocol) {
        self.userSettings = userSettings
        self.notificationService = notificationService
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView.emptyCollection()

        let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.scrollDirection = .vertical
        layout?.minimumInteritemSpacing = 0
        layout?.minimumLineSpacing = 0

        collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.alwaysBounceVertical = true

        return collectionView
    }()

    private lazy var collectionViewAdapter = SettingsAdapter(collectionView: collectionView)

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Settings"
        view.backgroundColor = Palette.primary
        view.addSubview(collectionView)

        setupNavigationBar(backButtonAction: #selector(handleBackTap))
        setupItems()

        collectionViewAdapter.onNotificationSwitch = { [unowned self] isOn in
            notificationService.setEnabled(isOn)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        navigationController?.navigationBar.fill()
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    // MARK: - Setup

    private func setupItems() {
        var items: [[SettingsItem]] = [
            [
                .init(id: .premium, type: .next)
            ],
            [
                .init(id: .notification, type: .switcher(userSettings.isNotificationEnabled)),
                .init(id: .privacy, type: .next),
                .init(id: .terms, type: .next),
                .init(id: .support, type: .next)
            ]
        ]

        if userSettings.isPremium {
            items.removeFirst()
        }

        collectionViewAdapter.items = items
    }

    // MARK: - Layout

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        collectionView.pin
            .horizontally()
            .top()
            .bottom()

        setupItems()
    }

    // MARK: - Actions

    @objc func handleBackTap() {
        onClose?()
    }

}

class SettingsAdapter: NSObject {

    var items: [[SettingsItem]] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    var onTap: ((SettingsItem.ID) -> Void)?

    var onNotificationSwitch: ((Bool) -> Void)?

    let collectionView: UICollectionView

    init(collectionView: UICollectionView) {
        self.collectionView = collectionView

        super.init()

        collectionView.register(SettingsCell.self, forCellWithReuseIdentifier: SettingsCell.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
    }

}

extension SettingsAdapter: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items[section].count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.section][indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SettingsCell.reuseIdentifier, for: indexPath) as! SettingsCell
        cell.model = .init(item: item)

        cell.onSwitch = { [weak self] isOn in
            if item.id == .notification {
                self?.onNotificationSwitch?(isOn)
            }
        }

        return cell
    }

}

extension SettingsAdapter: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.section][indexPath.item]
        onTap?(item.id)
    }

}

extension SettingsAdapter: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(
            width: collectionView.bounds.width,
            height: SettingsCell.height()
        )
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    }

}
