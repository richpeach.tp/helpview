import UIKit

extension SettingsCell {

    struct Model {
        let type: Type
        let theme: Theme
        let title: String
        let icon: UIImage?
        let hasSeparator: Bool

        init(item: SettingsItem) {
            switch item.id {
            case .premium:
                self.theme = .light
            default:
                self.theme = .dark
            }

            self.title = item.id.title
            self.icon = item.id.icon

            switch item.type {
            case .next:
                self.type = .next

            case .switcher(let bool):
                self.type = .switcher(bool)
            }

            switch item.id {
            case .notification, .privacy, .terms:
                self.hasSeparator = true
            default:
                self.hasSeparator = false
            }
        }
    }

}

class SettingsCell: UICollectionViewCell {

    enum Theme {
        case light // light background, dark content
        case dark // dark background, light content
    }

    enum `Type` {
        case switcher(Bool)
        case next
    }

    var onSwitch: ((Bool) -> Void)?

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup

    private func setup() {
        contentView.addSubview(containerView)
        containerView.addSubview(iconImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(rightIconImageView)
        containerView.addSubview(switcher)
        containerView.addSubview(separatorView)
    }

    // MARK: - Views

    lazy var containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Constants.cornerRadius
        return view
    }()

    lazy var iconImageView = UIImageView()

    lazy var titleLabel = UILabel(text: nil, textColor: nil, font: Constants.titleFont)

    lazy var rightIconImageView = UIImageView(image: UIImage(named: "settings-next")!.withRenderingMode(.alwaysTemplate))

    lazy var switcher: UISwitch = {
        let switcher = UISwitch()
        switcher.addTarget(self, action: #selector(handleSwitch), for: .valueChanged)
        return switcher
    }()

    lazy var separatorView = UIView(backgroundColor: UIColor.hex("#F49490"))

    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        let insets = Constants.contentInsets
        containerView.pin.all(Constants.containerInsets)

        let hasImage = iconImageView.image != nil

        iconImageView.pin
            .size(Constants.iconSize)
            .vCenter()
            .left(insets.left)

        titleLabel.pin
            .sizeToFit()
            .vCenter()
            .left(hasImage ?
                    iconImageView.frame.maxX + Constants.iconToTitle :
                    insets.left)

        switcher.pin
            .sizeToFit()
            .vCenter()
            .right(insets.right)

        rightIconImageView.pin
            .size(Constants.rightIconSize)
            .right(insets.right)
            .vCenter()

        separatorView.pin
            .left(insets.left)
            .right(insets.right)
            .height(1)
            .bottom()
    }

    static func height() -> CGFloat {
        62
    }

    // MARK: - Actions

    @objc private func handleSwitch() {
        onSwitch?(switcher.isOn)
    }

    // MARK: - Model

    var model: Model? {
        didSet {
            guard let model = model else { return }

            apply(theme: model.theme)
            titleLabel.text = model.title
            iconImageView.image = model.icon
            separatorView.isHidden = !model.hasSeparator

            switch model.type {
            case .switcher(let bool):
                switcher.isOn = bool
                switcher.isHidden = false
                rightIconImageView.isHidden = true

            case .next:
                switcher.isHidden = true
                rightIconImageView.isHidden = Screen.isIPhone5
            }
        }
    }

    private func apply(theme: Theme) {
        switch theme {
        case .light:
            containerView.backgroundColor = Constants.lightBackgroundColor
            titleLabel.textColor = Constants.darkContentColor
            iconImageView.tintColor = Constants.darkContentColor
            rightIconImageView.tintColor = Constants.darkContentColor
            switcher.tintColor = Constants.darkContentColor

        case .dark:
            containerView.backgroundColor = Constants.darkBackgroundColor
            titleLabel.textColor = Constants.lightContentColor
            iconImageView.tintColor = Constants.lightContentColor
            rightIconImageView.tintColor = Constants.lightContentColor
            switcher.tintColor = Constants.lightContentColor

        }
    }

}

private enum Constants {

    static let containerInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    static let contentInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)

    static let darkContentColor = Palette.black
    static let lightContentColor = Palette.white

    static let darkBackgroundColor = UIColor.clear
    static let lightBackgroundColor = Palette.white

    static let titleFont = Font.regular.withSize(16)
    static let cornerRadius: CGFloat = 10

    static let iconSize = CGSize(side: 30)
    static let rightIconSize = CGSize(side: 30)
    static let iconToTitle: CGFloat = 10

}
